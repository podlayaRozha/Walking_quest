﻿namespace QuestArea.Structures
{
    public struct Quest
    {
        public string id;
        public string questname;
        public string creator;
        public float rating;
        public string creatorAvatar;
        public string status;
        public string image;
        public string description;
    }
}


