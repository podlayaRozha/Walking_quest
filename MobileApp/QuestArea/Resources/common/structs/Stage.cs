﻿namespace QuestArea.Structures
{
    public struct Stage
    {
        public string title;
        public string description;
    }
}