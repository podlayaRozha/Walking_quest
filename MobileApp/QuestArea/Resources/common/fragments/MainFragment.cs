﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using QuestAreaAPI;
using System.Collections.Generic;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Views.InputMethods;
using DataStore;

namespace QuestArea
{
    public class FragmentMain : Fragment
    {
        ListView mainScroll;
        ProgressBar progressBar;
        ProgressBar avatarProgressBar;
        QAAPI api;
        LinearLayout errorLayout;
        TextView errorText;
        Button updateButton;
        List<listQuest> quests;
        SearchView searchQuestView;
        DrawerLayout drawer;
        NavigationView navView;
        SwipeRefreshLayout swLayout;

        bool questsLoaded = false;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
            Application.Context.SetTheme(Resource.Style.MainTheme);
            var view = inflater.Inflate(Resource.Layout.cardlist, container, false);
          
            mainScroll = view.FindViewById<ListView>(Resource.Id.questList);
            progressBar = view.FindViewById<ProgressBar>(Resource.Id.progressBar);
            avatarProgressBar = view.FindViewById<ProgressBar>(Resource.Id.avatarProgressBar);
            errorLayout = view.FindViewById<LinearLayout>(Resource.Id.mainViewError);
            errorText = view.FindViewById<TextView>(Resource.Id.errorText);
            updateButton = view.FindViewById<Button>(Resource.Id.errorButton);
            searchQuestView = view.FindViewById<SearchView>(Resource.Id.questSearch);
            drawer = view.FindViewById<DrawerLayout>(Resource.Id.navigationDrawer);
            navView = view.FindViewById<NavigationView>(Resource.Id.navView);
            swLayout = view.FindViewById<SwipeRefreshLayout>(Resource.Id.swiperefresh);

            swLayout.Refresh += (e, sender) =>
            {
                Update();
            };

            mainScroll.ItemClick += (e, sender) =>
            {
                var fragmentTx = FragmentManager.BeginTransaction();
                var questID = sender.View.FindViewById<TextView>(Resource.Id.cacheQuestText).Text;
                var quest = Newtonsoft.Json.JsonConvert.SerializeObject(quests.Find(x => x._id == questID));
                var sharedPrefs = Application.Context.GetSharedPreferences("state", FileCreationMode.Private);
                
                sharedPrefs.Edit().PutString("quest", quest).Commit();

                fragmentTx.SetTransition(FragmentTransit.FragmentOpen);
                fragmentTx.Replace(Resource.Id.rootLayout, new FragmentQuesPage(), "Current");
                fragmentTx.AddToBackStack(null);
                fragmentTx.Commit();
            };

            api = QAAPI.GetInstance;
            
            if (!Tools.IsInternetOn())
            {
                OnSuccesQuestRequest();
                return view;
            }

            var loadingHandler = new Handler();
            System.Action generateQuestList = OnSuccesQuestRequest;
            System.Action showError = ShowConnectionError;

            api.SendDataComplete += () => {
                if (api.Quests != null)
                {
                    loadingHandler.Post(generateQuestList);
                }
            };

            api.SendDataFailed += () =>
            {
                loadingHandler.Post(showError);
                return;
            };
   
            api.GetFeed();
            
            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnViewStateRestored(savedInstanceState);

            if(savedInstanceState != null)
            {
                var test = savedInstanceState.GetString("test");
            }
        }

        void OnSuccesQuestRequest()
        {
            quests = new List<listQuest>();
            
            if(api.Quests != null)
            {
                //Генерация списка квестов
                foreach (var quest in api.Quests)
                {
                    quests.Add(new listQuest
                    {
                        _id = quest.id,
                        _questName = quest.questname,
                        _creatorName = quest.creator,
                        _rating = quest.rating,
                        _creatorAvatar = quest.creatorAvatar,
                        _imageUrl = quest.image,
                        _description = quest.description,
                        _status = quest.status
                    });
                }
            }
            else
            {
                quests = (List<listQuest>)Storage.Get<List<listQuest>>("main");
            }

            if (quests == null)
            {
                //Если квесты не пришли ни с сервера, ни с кэша - показываю ошибку
                ShowConnectionError();
                return;
            }

            var questsAdapter = new QuestAdapter(Application.Context, quests);

            progressBar.Visibility = ViewStates.Gone;
            mainScroll.Visibility = ViewStates.Visible;
            swLayout.Visibility = ViewStates.Visible;
            mainScroll.Adapter = questsAdapter;
            questsLoaded = true;

            Storage.Store("main", quests);

            if(swLayout.Refreshing)
            {
                swLayout.Refreshing = false;
            }
        }

        void ShowConnectionError()
        {
            if (api.ErrorMessage != null)
                Toast.MakeText(Application.Context, api.ErrorMessage, ToastLength.Short).Show();

            if (questsLoaded)
                return;

            errorLayout.Visibility = ViewStates.Visible;
            errorText.Text = Application.Context.GetString(Resource.String.connection_problems);
            updateButton.Click += Update;
            progressBar.Visibility = ViewStates.Gone;
            swLayout.Visibility = ViewStates.Gone;
        }

        public void QuestSearch(string query)
        {
            //Скрываю экранную клавиатуру
            InputMethodManager inputManager = (InputMethodManager)Application.Context.GetSystemService(Context.InputMethodService);
            inputManager.HideSoftInputFromWindow(Activity.CurrentFocus.WindowToken, 0);

            errorLayout.Visibility = ViewStates.Gone;
            progressBar.Visibility = ViewStates.Visible;
            mainScroll.Visibility = ViewStates.Invisible;

            api.Search(query);
        }

        public void Update(object sender = null, object args = null)
        {

            if (!Tools.IsInternetOn())
                return;

            errorLayout.Visibility = ViewStates.Gone;
            mainScroll.Visibility = ViewStates.Invisible;
            swLayout.Visibility = ViewStates.Gone;

            var loadingHandler = new Handler();
            System.Action generateQuestList = OnSuccesQuestRequest;

            api.SendDataComplete += () => {
                if (api.Quests != null)
                {
                    loadingHandler.Post(generateQuestList);
                }
            };

            api.SendDataFailed += () =>
            {
                swLayout.Refreshing = false;
                ShowConnectionError();
            };

            api.GetFeed();
        }
    }
}
