﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using QuestAreaAPI;
using System.Collections.Generic;
using Android.Support.V4.Widget;
using DataStore;

namespace QuestArea
{
    public class FragmentStory : Fragment
    {
        ListView mainScroll;
        ProgressBar progressBar;
        QAAPI api;
        LinearLayout errorLayout;
        TextView errorText;
        Button updateButton;
        List<listQuest> quests;
        List<listQuest> questsSearchDuplicate;
        QuestAdapter questsAdapter;
        SwipeRefreshLayout swLayout;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Application.Context.SetTheme(Resource.Style.MainTheme);
            var view = inflater.Inflate(Resource.Layout.cardlist, container, false);
            
            mainScroll = view.FindViewById<ListView>(Resource.Id.questList);
            progressBar = view.FindViewById<ProgressBar>(Resource.Id.progressBar);
            errorLayout = view.FindViewById<LinearLayout>(Resource.Id.mainViewError);
            errorText = view.FindViewById<TextView>(Resource.Id.errorText);
            updateButton = view.FindViewById<Button>(Resource.Id.errorButton);
            swLayout = view.FindViewById<SwipeRefreshLayout>(Resource.Id.swiperefresh);
            questsSearchDuplicate = new List<listQuest>();

            swLayout.Refresh += (e, sender) =>
            {
                Update();
            };

            mainScroll.ItemClick += (e, sender) =>
            {
                var fragmentTx = FragmentManager.BeginTransaction();
                var questID = sender.View.FindViewById<TextView>(Resource.Id.cacheQuestText).Text;
                var quest = Newtonsoft.Json.JsonConvert.SerializeObject(quests.Find(x => x._id == questID));
                var sharedPrefs = Application.Context.GetSharedPreferences("state", FileCreationMode.Private);

                sharedPrefs.Edit().PutString("quest", quest).Commit();

                fragmentTx.SetTransition(FragmentTransit.FragmentFade);
                fragmentTx.Replace(Resource.Id.rootLayout, new FragmentQuesPage(), "Current");
                fragmentTx.AddToBackStack(null);
                fragmentTx.Commit();
            };

            api = QAAPI.GetInstance;

            if (!Tools.IsInternetOn())
            {
                ShowConnectionError();
                return view;
            }

            var loadingHandler = new Handler();
            System.Action generateQuestList = OnSuccesQuestRequest;
            System.Action showError = ShowConnectionError;

            api.SendDataComplete += () => {
                loadingHandler.Post(generateQuestList);
            };

            api.SendDataFailed += () =>
            {
                loadingHandler.Post(showError);
                return;
            };

            api.GetStory();

            return view;
        }

        void OnSuccesQuestRequest()
        {
            quests = new List<listQuest>();

            if (api.Quests != null)
            {
                //Генерация списка квестов
                foreach (var quest in api.Quests)
                {
                    quests.Add(new listQuest
                    {
                        _id = quest.id,
                        _questName = quest.questname,
                        _creatorName = quest.creator,
                        _rating = quest.rating,
                        _creatorAvatar = quest.creatorAvatar,
                        _imageUrl = quest.image,
                        _description = quest.description,
                        _status = quest.status
                    });
                }
            }
            else
            {
                quests = (List<listQuest>)Storage.Get<List<listQuest>>("story");
            }

            if (quests == null)
            {
                //Если квесты не пришли ни с сервера, ни с кэша - показываю ошибку
                ShowConnectionError();
                return;
            }

            questsAdapter = new QuestAdapter(Application.Context, quests);

            progressBar.Visibility = ViewStates.Gone;
            mainScroll.Visibility = ViewStates.Visible;
            swLayout.Visibility = ViewStates.Visible;
            mainScroll.Adapter = questsAdapter;

            questsSearchDuplicate.Clear();
            questsSearchDuplicate.AddRange(quests);
            Storage.Store("story", quests);

            if (swLayout.Refreshing)
            {
                swLayout.Refreshing = false;
            }
        }

        void ShowConnectionError()
        {
            if (api.ErrorMessage != null)
                Toast.MakeText(Application.Context, api.ErrorMessage, ToastLength.Short).Show();

            errorLayout.Visibility = ViewStates.Visible;
            errorText.Text = Application.Context.GetString(Resource.String.connection_problems);
            updateButton.Click += Update;
            progressBar.Visibility = ViewStates.Gone;
            swLayout.Visibility = ViewStates.Gone;
        }

        public void Update(object sender = null, object args = null)
        {
            if (!Tools.IsInternetOn())
                return;

            errorLayout.Visibility = ViewStates.Gone;
            mainScroll.Visibility = ViewStates.Invisible;
            swLayout.Visibility = ViewStates.Gone;

            var loadingHandler = new Handler();
            System.Action generateQuestList = OnSuccesQuestRequest;

            api.SendDataComplete += () => {
                if (api.Quests != null)
                {
                    loadingHandler.Post(generateQuestList);
                }
            };

            api.SendDataFailed += () =>
            {
                swLayout.Refreshing = false;
                ShowConnectionError();
            };

            api.GetStory();
        }

        public void QuestSearch(string query)
        {
            var result  = new List<listQuest>();

            for (int i = 0; i < questsSearchDuplicate.Count; i++)
            {
                if (questsSearchDuplicate[i]._questName.Contains(query))
                {
                    result.Add(questsSearchDuplicate[i]);
                }
            }

            if (result.Count <= 0)
            {
                Toast.MakeText(Activity, Resource.String.not_found, ToastLength.Short).Show();
                return;
            }

            quests.Clear();
            quests.AddRange(result);

            questsAdapter.NotifyDataSetChanged();
        }
    }
}
