﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using QuestAreaAPI;
using Android.Graphics;

namespace QuestArea
{
    public class FragmentQuesPage : Fragment
	{
		QAAPI api;
        listQuest quest;
        TextView questName;
        SearchView searchView;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            Application.Context.SetTheme(Resource.Style.MainTheme);
            var view = inflater.Inflate(Resource.Layout.questPage, container, false);

            questName = Activity.FindViewById<TextView>(Resource.Id.toolBarText);
            searchView = Activity.FindViewById<SearchView>(Resource.Id.questSearch);

			var questCreator = view.FindViewById<TextView>(Resource.Id.creatorName);
			var questDescription = view.FindViewById<TextView>(Resource.Id.questText);
            var ratingBar = view.FindViewById<RatingBar>(Resource.Id.ratingBar);
            var creatorAvatar = view.FindViewById<ImageView>(Resource.Id.creatorAvatar);
            var startQuestBtn = view.FindViewById<Button>(Resource.Id.questStartButton);
            var ratingDigital = view.FindViewById<TextView>(Resource.Id.ratingDigital);
            var questImage = view.FindViewById<ImageView>(Resource.Id.questImage);
            var addToFavouritesButton = view.FindViewById<ImageButton>(Resource.Id.addToFavouriteButton);

            api = QAAPI.GetInstance;

            var sharedPrefs = Application.Context.GetSharedPreferences("state", FileCreationMode.Private);
            var json = sharedPrefs.GetString("quest", null);

            quest = Newtonsoft.Json.JsonConvert.DeserializeObject<listQuest>(json);

            Java.Lang.Runnable endAction = new Java.Lang.Runnable(() => {
                searchView.Visibility = ViewStates.Gone;
            });

            questName.Animate()
                .RotationXBy(360f)
                .WithEndAction(endAction)
                .Start();

            searchView.Animate().TranslationX(200f).Start();

            questName.Text = quest._questName;
            questCreator.Text = quest._creatorName;
            questDescription.Text = quest._description;
            ratingBar.Rating = quest._rating;
            ratingDigital.Text = quest._rating.ToString();

            ImageProcessing.SetRoundedImageViewAsync(Application.Context, quest._creatorAvatar, creatorAvatar);

            ImageProcessing.SetImageViewFromUrlAsync(Application.Context, quest._imageUrl, questImage);

            addToFavouritesButton.Click += (e, sender) =>
            {
                var id = Convert.ToInt32(quest._id);

                var favouriteAddedBitmap = BitmapFactory.DecodeResource(Resources, Resource.Drawable.baseline_favorite_black_48dp);  
                addToFavouritesButton.SetImageBitmap(favouriteAddedBitmap);

                api.AddToFavourites(id);
            };

            if(!Tools.IsLoggedIn)
            {
                addToFavouritesButton.Visibility = ViewStates.Gone;
            }

            if (quest._status == "complite")
            {
                startQuestBtn.Enabled = false;
                startQuestBtn.SetText(Resource.String.completed);
            }

            if (quest._status == "taken")
            {
                startQuestBtn.SetText(Resource.String.continue_quest);
            }

            startQuestBtn.Click += onQuestStartClick;
            return view;
        }

        public override void OnPause()
        {
            base.OnPause();

            Java.Lang.Runnable endAction = new Java.Lang.Runnable(() => {
                searchView.Visibility = ViewStates.Visible;
                searchView.Animate().TranslationX(0f).Start();
            });
            
            questName.Animate()
                .RotationXBy(360f)
                .WithEndAction(endAction)
                .Start();
            
        }
        
		public void onQuestStartClick(object sender, EventArgs e)
		{
            //Если пользователь неавторизован, предлагаю ему регистрацию/авторизацию
            if(Application.Context.GetSharedPreferences("API", FileCreationMode.Private).GetString("access_token", null) == null)
            {
                var dialogBuilder = new AlertDialog.Builder(Activity);
                dialogBuilder.SetMessage(Resources.GetString(QuestArea.Resource.String.login_required));

                dialogBuilder.SetPositiveButton("Ok", delegate
                {
                    StartActivity(new Intent(Application.Context, typeof(signIn)));
                });

                dialogBuilder.SetNegativeButton(Resource.String.cancel, delegate
                {
                    dialogBuilder.Dispose();
                });

                dialogBuilder.Show();
                return;
            }

            //Если квест уже взят - запускаю прохождение
            if(quest._status == "taken")
            {
                try
                {
                    Intent intent = new Intent(Application.Context, typeof(Stages));
                    intent.PutExtra("questID", quest._id);
                    StartActivity(intent);
                }
                catch (Exception ex)
                {
                    DebugLog.LogFile.Print(ex.Message);
                }
                return;
            }

            //Если квест не взят - стартую квест
            if (quest._status == "neutral")
            {
                api = QAAPI.GetInstance;

                var activity = (MainActivity)Activity;
                
                var loadingHandler = new Handler();

                System.Action fail = () =>
                {
                    string errorText = api.ErrorMessage;

                    if (errorText == null)
                        errorText = GetString(Resource.String.other_error);

                    Toast.MakeText(Application.Context, errorText, ToastLength.Short).Show();
                    activity.HideQuestStartDialog();
                    return;
                };

                System.Action success = async () =>
                {
                    Toast.MakeText(Application.Context, Resource.String.quest_start_confirmed, ToastLength.Long).Show();
                    Tools.RestartAndOpenActivity(Application.Context, typeof(MainActivity), true);
                };

                api.SendDataComplete += () => { loadingHandler.Post(success); };
                api.SendDataFailed += () => { loadingHandler.Post(fail); };
                
                System.Action action = delegate {
                    api.StartQuest(quest._id);
                };
                activity.ShowQuestStartDialog(action);
                                
                return;
            }
        }
	}
}

