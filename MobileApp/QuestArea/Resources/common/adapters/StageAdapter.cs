﻿using Android.Content;
using Android.Views;
using Android.Widget;
using QuestArea.Structures;

namespace QuestArea
{
    class StageAdapter : BaseAdapter
    {
        LayoutInflater _inflater;
        Stage[] _stages;

        public StageAdapter(Context ctx, Stage[] StageList)
        {
            _stages = StageList;
            _inflater = (LayoutInflater)ctx.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count => _stages.Length;

        public override Java.Lang.Object GetItem(int position) => null;

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;

            if (view == null)
            {
                view = _inflater.Inflate(Resource.Layout.stageCard, parent, false);
            }

            var stageCaption = view.FindViewById<TextView>(Resource.Id.stageName);
            var stageDescription = view.FindViewById<TextView>(Resource.Id.stageDescription);
            var stage = (Stage)_stages.GetValue(position);

            stageCaption.Text = stage.title;
            stageDescription.Text = stage.description;

            return view;
        }
    }
}
