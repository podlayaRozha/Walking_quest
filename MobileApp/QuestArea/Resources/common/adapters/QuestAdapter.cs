﻿using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using QuestAreaAPI;

namespace QuestArea
{
    class listQuest
    {
        public string _id { get; set; }
        public string _questName { get; set; }
        public string _creatorAvatar { get; set; }
        public string _creatorName { get; set; }
        public float _rating { get; set; }
        public string _imageUrl { get; set; }
        public string _status { get; set; }
        public string _description { get; set; }
	}

    class QuestAdapter : BaseAdapter
    {
        List<listQuest> _quests;
        Context _context;
        LayoutInflater _inflater;
        View[] _view;

        public QuestAdapter(Context context, List<listQuest> quests)
        {
            _context = context;
            _quests = quests;
            _view = new View[_quests.Count];
            _inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count => _quests.Count;

        public override Java.Lang.Object GetItem(int position) => _view[position];

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            _view[position] = convertView;

            if (_view[position] == null)
            {
                _view[position] = _inflater.Inflate(Resource.Layout.questCard, parent, false);
            }

            var questName = _view[position].FindViewById<TextView>(Resource.Id.questName);
            var questCreator = _view[position].FindViewById<TextView>(Resource.Id.creatorName);
            var ratingBar = _view[position].FindViewById<RatingBar>(Resource.Id.ratingBar);
            var image = _view[position].FindViewById<ImageView>(Resource.Id.questFoto);
			var progressBar = _view[position].FindViewById<ProgressBar>(Resource.Id.ImageProgressBar);
            var creatorImage = _view[position].FindViewById<ImageView>(Resource.Id.creatorAvatar);
            var cacheView = _view[position].FindViewById<TextView>(Resource.Id.cacheQuestText);

			questName.Text = _quests[position]._questName;
            questCreator.Text = _quests[position]._creatorName;
            ratingBar.Rating = _quests[position]._rating;
			cacheView.Text = _quests[position]._id;

            ImageProcessing.SetImageViewFromUrlAsync(_context, _quests[position]._imageUrl, image, progressBar);
            ImageProcessing.SetRoundedImageViewAsync(_context, _quests[position]._creatorAvatar, creatorImage);

            return _view[position];
        }
    }
}