﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using QuestAreaAPI;

namespace QuestArea
{
    [Activity(Label = "QuestArea", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class signIn : Activity
	{
		QAAPI api;
		ProgressDialog loadingDialog;
		Toast succesToast;
		Toast errorToast;
		TextView errorText;
        ISharedPreferences sharedPrefs;

		protected override void OnCreate(Bundle savedInstanceState)
		{
            SetTheme(Resource.Style.MainTheme);
            base.OnCreate(savedInstanceState);

			RequestWindowFeature(WindowFeatures.NoTitle);
			SetContentView(Resource.Layout.signIn);

			FindViewById<Button>(Resource.Id.signInButton).Click += OnLoginButtonClick;

            sharedPrefs = GetSharedPreferences("settings", FileCreationMode.Private);
            Tools.SetCustomBackIfExist(this, sharedPrefs);

            api = QAAPI.GetInstance;
		}

		//Обработчики
		//Диалоговое окно регистрации
		[Java.Interop.Export("onSignUpClick")]
		public void OnSignUpClick(View v)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			LayoutInflater iInflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
			var dialog = iInflater.Inflate(Resource.Layout.dialog_signUp, null);

			builder.SetView(dialog);

			builder.SetPositiveButton(Resource.String.sign_up, delegate
			{
				var email = dialog.FindViewById<EditText>(Resource.Id.regEmail);
				var login = dialog.FindViewById<EditText>(Resource.Id.regLogin);
				var password = dialog.FindViewById<EditText>(Resource.Id.regPassword);
				errorText = FindViewById<TextView>(Resource.Id.loginErrorText);

				succesToast = Toast.MakeText(this, Resource.String.register_succes, ToastLength.Long);
				errorToast = Toast.MakeText(this, Resource.String.register_succes, ToastLength.Long);

                var validator = new Validator();

				if (!validator.ValidateAllData(email.Text, password.Text, login.Text))
				{
					Toast.MakeText(ApplicationContext, validator.ErrorStringID, ToastLength.Short).Show();
					return;
				}

				loadingDialog = ProgressDialog.Show(this, "", Application.Context.GetString(Resource.String.loading), true);
				errorText.Text = "";

				api.RegisterAsync(email.Text, login.Text, password.Text);
				api.SendDataComplete += OnSuccesRequest;
				api.SendDataFailed += OnFailRequest;
			});
			builder.Show();
		}

		void OnLoginButtonClick(object sender, object args)
		{
			var login = FindViewById<EditText>(Resource.Id.login);
			var password = FindViewById<EditText>(Resource.Id.password);
			errorText = FindViewById<TextView>(Resource.Id.loginErrorText);

			succesToast = Toast.MakeText(this, Resource.String.login_succes, ToastLength.Long);
			errorToast = Toast.MakeText(this, Resource.String.register_succes, ToastLength.Long);

            var validator = new Validator();

            if (!validator.ValidateEmpty(login.Text, password.Text))
			{
				errorText.Text = Application.Context.GetString(Resource.String.empty_requered_filed);
				return;
			}

			loadingDialog = ProgressDialog.Show(this, "", Application.Context.GetString(Resource.String.loading), true);
			errorText.Text = "";

			api.LoginAsync(login.Text, password.Text);

			api.SendDataComplete += OnSuccesRequest;

			api.SendDataFailed += OnFailRequest;
		}

		void OnSuccesRequest()
		{
			loadingDialog.Dismiss();

			succesToast.Show();

            Tools.SaveAuth(api.Token);

			Finish();
			api = null;
			Tools.RestartAndOpenActivity(ApplicationContext, typeof(MainActivity));
		}

		void OnFailRequest()
		{
			loadingDialog.Dismiss();
			errorToast.SetText(api.ErrorMessage);
			errorToast.Show();
			errorText.Text = api.ErrorMessage;
		}
	}
}

