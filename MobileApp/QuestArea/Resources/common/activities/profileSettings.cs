﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Graphics;
using QuestAreaAPI;
using Android.Provider;
using Android.Runtime;
using Android.Database;
//using Data;

namespace QuestArea
{
    [Activity(Label = "QuestArea", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class profileSettings : Activity
	{
		ArrayAdapter<String> profileArrayAdapter;
        ArrayAdapter<String> appearanceArrayAdapter;
		QAAPI api;
        const int IMGREQUESTCODE = 1;

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            
            if(requestCode == IMGREQUESTCODE && resultCode == Result.Ok)
            {
                var sharedPrefs = GetSharedPreferences("settings", FileCreationMode.Private);
                Android.Net.Uri uri = data.Data;
                var realPath = GetActualPathFromFile(uri);
                sharedPrefs.Edit().PutString("backroundImage", realPath).Commit();
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
		{
            SetTheme(Resource.Style.MainTheme);
            base.OnCreate(savedInstanceState);

			RequestWindowFeature(WindowFeatures.NoTitle);
			SetContentView(Resource.Layout.profileSettings);

			var profileSettingsList = FindViewById<ListView>(Resource.Id.profileSettingsList);
			var accountSettings = FindViewById(Resource.Id.accountSettingsCard);
            var appearanceSettingsList = FindViewById<ListView>(Resource.Id.appearanceSettingsList);

            FindViewById<LinearLayout>(Resource.Id.settingsLayout).SetBackgroundColor(Color.DarkGray);

            var token = Application.Context.GetSharedPreferences("API", FileCreationMode.Private).GetString("access_token", null);

			if(token == null)
			{
				accountSettings.Visibility = ViewStates.Gone;
			}

			String[] profileSettingItems = {
				Application.Context.GetString(Resource.String.change_login),
				Application.Context.GetString(Resource.String.change_email),
				Application.Context.GetString(Resource.String.change_password),
				Application.Context.GetString(Resource.String.logout)
			};

            String[] visualSettingsItems = 
            {
                Application.Context.GetString(Resource.String.change_backround),
                Application.Context.GetString(Resource.String.default_appearance)
            };

			profileArrayAdapter = new ArrayAdapter<string>(this, Resource.Layout.TextViewItem, profileSettingItems);
            appearanceArrayAdapter = new ArrayAdapter<string>(this, Resource.Layout.TextViewItem, visualSettingsItems);


            profileSettingsList.Adapter = profileArrayAdapter;
            appearanceSettingsList.Adapter = appearanceArrayAdapter;

            profileSettingsList.ItemClick += (sender, args) =>
			{
				var itemText = profileArrayAdapter.GetItem(args.Position);
				api = QAAPI.GetInstance;

                if (itemText == Application.Context.GetString(Resource.String.logout))
				{
					var sharedPrefs = Application.Context.GetSharedPreferences("API", FileCreationMode.Private);
					sharedPrefs.Edit().PutString("access_token", null).Commit();
                    sharedPrefs.Edit().PutString("login", null).Commit();

                    Tools.RestartAndOpenActivity(ApplicationContext, typeof(MainActivity));
				}

				if (itemText == Application.Context.GetString(Resource.String.change_login))
				{
                    ChangeUserData("login", Resource.String.reg_login, false);
				}

				if (itemText == Application.Context.GetString(Resource.String.change_email))
				{
					ChangeUserData("email", Resource.String.email, false);
				}

				if (itemText == Application.Context.GetString(Resource.String.change_password))
				{
					ChangeUserData("password", Resource.String.reg_password ,true);
				}
            };
            appearanceSettingsList.ItemClick += (sender, args) =>
            {
                var itemText = appearanceArrayAdapter.GetItem(args.Position);

                if(itemText ==  Application.Context.GetString(Resource.String.change_backround))
                {
                    var dialog = new AlertDialog.Builder(this);
                    dialog.SetMessage(Resource.String.storage_support);
                    dialog.SetPositiveButton("Ok", delegate
                    {
                        Intent intent = new Intent().SetType("image/*").SetAction(Intent.ActionGetContent);

                        StartActivityForResult(Intent.CreateChooser(intent, "Select"), IMGREQUESTCODE);
                    });
                    dialog.Show();
                }

                if(itemText == Application.Context.GetString(Resource.String.default_appearance))
                {
                    var sharedPrefs = GetSharedPreferences("settings", FileCreationMode.Private);
                    sharedPrefs.Edit().PutString("backroundImage", null).Commit();
                }
            };
		}

		void ChangeUserData(string api_method, int hint_text_id, bool hideInput)
		{

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			LayoutInflater iInflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
			var dialog = iInflater.Inflate(Resource.Layout.dialog_changeUserInfo, null);

			var dialogText = dialog.FindViewById<TextView>(Resource.Id.dlgField);
			dialogText.Hint = Application.Context.GetString(hint_text_id);
			if(!hideInput)
				dialogText.InputType = Android.Text.InputTypes.ClassText;

			builder.SetView(dialog);

			builder.SetPositiveButton("Ok", delegate
			{
                var validator = new Validator();

				if (!validator.ValidateEmpty(dialogText.Text))
				{
					Toast.MakeText(this, validator.ErrorStringID, ToastLength.Short).Show();
					return;
				}

				api = QAAPI.GetInstance;
				var loadingHandler = new Handler();
				System.Action doAferLoad = () => OnSuccesRequest(api_method, dialogText.Text);
				System.Action doAfterFailLoad = OnFailRequest;

				api.SendDataComplete += () => loadingHandler.Post(doAferLoad);
				api.SendDataFailed += () => loadingHandler.Post(doAfterFailLoad);
				api.ChangeUserInfoAsync(api_method, dialogText.Text);
			});
			builder.Show();
		}

		void OnSuccesRequest(string field, string data)
		{
			Toast.MakeText(this, Application.Context.GetString(Resource.String.succes), ToastLength.Long).Show();
		}

		void OnFailRequest()
		{
			Toast.MakeText(this, api.ErrorMessage, ToastLength.Long).Show();
		}


        private string GetActualPathFromFile(Android.Net.Uri uri)
        {
            bool isKitKat = Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat;

            if (isKitKat && DocumentsContract.IsDocumentUri(this, uri))
            {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri))
                {
                    string docId = DocumentsContract.GetDocumentId(uri);

                    char[] chars = { ':' };
                    string[] split = docId.Split(chars);
                    string type = split[0];
                    
                    if ("primary".Equals(type, StringComparison.OrdinalIgnoreCase))
                    {
                        return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri))
                {
                    string id = DocumentsContract.GetDocumentId(uri);

                    Android.Net.Uri contentUri = ContentUris.WithAppendedId(
                                    Android.Net.Uri.Parse("content://downloads/public_downloads"), long.Parse(id));

                    //System.Diagnostics.Debug.WriteLine(contentUri.ToString());

                    return getDataColumn(this, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri))
                {
                    String docId = DocumentsContract.GetDocumentId(uri);

                    char[] chars = { ':' };
                    String[] split = docId.Split(chars);

                    String type = split[0];

                    Android.Net.Uri contentUri = null;
                    if ("image".Equals(type))
                    {
                        contentUri = MediaStore.Images.Media.ExternalContentUri;
                    }
                    else if ("video".Equals(type))
                    {
                        contentUri = MediaStore.Video.Media.ExternalContentUri;
                    }
                    else if ("audio".Equals(type))
                    {
                        contentUri = MediaStore.Audio.Media.ExternalContentUri;
                    }

                    String selection = "_id=?";
                    String[] selectionArgs = new String[]
                    {
                split[1]
                    };

                    return getDataColumn(this, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.LastPathSegment;

                return getDataColumn(this, uri, null, null);
            }
            // File
            else if ("file".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return uri.Path;
            }

            return null;
        }

        public static String getDataColumn(Context context, Android.Net.Uri uri, String selection, String[] selectionArgs)
        {
            ICursor cursor = null;
            String column = "_data";
            String[] projection =
            {
        column
    };

            try
            {
                cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs, null);
                if (cursor != null && cursor.MoveToFirst())
                {
                    int index = cursor.GetColumnIndexOrThrow(column);
                    return cursor.GetString(index);
                }
            }
            finally
            {
                if (cursor != null)
                    cursor.Close();
            }
            return null;
        }

        //Whether the Uri authority is ExternalStorageProvider.
        public static bool isExternalStorageDocument(Android.Net.Uri uri)
        {
            return "com.android.externalstorage.documents".Equals(uri.Authority);
        }

        //Whether the Uri authority is DownloadsProvider.
        public static bool isDownloadsDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.downloads.documents".Equals(uri.Authority);
        }

        //Whether the Uri authority is MediaProvider.
        public static bool isMediaDocument(Android.Net.Uri uri)
        {
            return "com.android.providers.media.documents".Equals(uri.Authority);
        }

        //Whether the Uri authority is Google Photos.
        public static bool isGooglePhotosUri(Android.Net.Uri uri)
        {
            return "com.google.android.apps.photos.content".Equals(uri.Authority);
        }
    }
}
