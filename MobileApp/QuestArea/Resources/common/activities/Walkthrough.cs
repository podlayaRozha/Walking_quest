﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Widget;
using Uri = Android.Net.Uri;
using Environment = Android.OS.Environment;
using QuestAreaTools;
using Android.Locations;
using Android.Runtime;
using System.Linq;
using System.Threading.Tasks;
using QuestAreaAPI;

namespace QuestArea
{
    [Activity(Label = "QuestArea", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class Walkthrough : Activity, ILocationListener
	{
		CameraImplementation cam;
		LocationManager _locationManager;
        Bitmap imageToCompare;
        Bitmap referenceImage;
        ProgressDialog loadingDialog;
        QAAPI api;
        AlertDialog.Builder errorDialog;
        AlertDialog.Builder helpDialogBuilder;
        AlertDialog gpsRequestDialog;
        AlertDialog helpDialog;

        string _locationProvider;
		double Longitude;
		double Latitude;
        string questID;
		int RequestLocationId;
        bool pictureTaken = false;
        bool TakePictureCanceled = false;
        const decimal LOCATION_ACCURACY = 0.0009m;
        const int COMPARE_COEF = 43;


        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
            base.OnActivityResult(requestCode, resultCode, data);

			// Make it available in the gallery

			Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
			Uri contentUri = Uri.FromFile(cam.File);
			mediaScanIntent.SetData(contentUri);
			SendBroadcast(mediaScanIntent);
            errorDialog = new AlertDialog.Builder(this);

            errorDialog.SetPositiveButton(Resource.String.try_again, delegate
            {
                TakeAPicture();
                errorDialog.Dispose();
            });
            errorDialog.SetNegativeButton(Resource.String.cancel, delegate
            {
                loadingDialog.Dismiss();
                base.OnBackPressed();
            });

            helpDialog.Dismiss();
            // Display in ImageView. We will resize the bitmap to fit the display
            // Loading the full sized image will consume to much memory 
            // and cause the application to crash.

            cam.Bitmap = cam.File.Path.LoadAndResizeBitmap(800, 600);
            imageToCompare = cam.Bitmap;

            if(imageToCompare == null)
            {
                errorDialog.SetMessage(this.Resources.GetString(Resource.String.capture_error));
                errorDialog.Show();
                return;
            }

            try
            {
                var compareResult = OpenCVImplementation.compareImage(imageToCompare, referenceImage);
                if (compareResult < COMPARE_COEF)
                {
                    errorDialog.SetMessage(this.Resources.GetString(Resource.String.compare_error));
                    errorDialog.Show();
                    return;
                }
            }
            catch (Exception e)
            {
                DebugLog.LogFile.Print(e.Message);

                errorDialog.SetMessage(this.Resources.GetString(Resource.String.compare_error));
                errorDialog.Show(); 
                return;
            }

            loadingDialog.SetMessage(Application.Context.GetString((Resource.String.loading_data)));

            api = QAAPI.GetInstance;
            var loadingHandler = new Handler();
            System.Action fail = () =>
            {
                errorDialog.SetMessage(api.ErrorMessage);
                errorDialog.Show();
                return;
            };

            System.Action success = async () =>
            {
                loadingDialog.Dismiss();
                var button = FindViewById<Button>(Resource.Id.quest_end_btn);
                var succesLayout = FindViewById<LinearLayout>(Resource.Id.CompliteLayout);
                var completeText = FindViewById<TextView>(Resource.Id. questCompliteText);
                var rateBar = FindViewById<RatingBar>(Resource.Id.end_quest_mark);
                succesLayout.Visibility = Android.Views.ViewStates.Visible;
                completeText.Text = String.Format(GetString(Resource.String.congratulations), api.TimeSpent);
                button.Click += (e, sender) => {
                    api.RateQuest(questID, rateBar.Rating);
                    Tools.RestartAndOpenActivity(this, typeof(MainActivity));
                };
            };

            api.SendDataComplete += () => { loadingHandler.Post(success); };

            api.SendDataFailed += () => { loadingHandler.Post(fail); };

            api.CompliteQuest(questID);
		}

		protected override void OnCreate(Bundle bundle)
		{
            SetTheme(Resource.Style.MainTheme);
            base.OnCreate(bundle);
			SetContentView(Resource.Layout.Walkthrough);

            var questEndButton = FindViewById<Button>(Resource.Id.quest_end_btn);
            var endQuestRateBar = FindViewById<RatingBar>(Resource.Id.end_quest_mark);
            questEndButton.Click += (e, sender) => {
                api.RateQuest(questID, endQuestRateBar.Rating);
            };

            loadingDialog = ProgressDialog.Show(this, Application.Context.GetString(Resource.String.loading_title), Application.Context.GetString(Resource.String.loading_data), true);
            loadingDialog.SetCancelable(true);
            loadingDialog.CancelEvent += (e, sender) => {
                base.OnBackPressed();
            };

            errorDialog = new AlertDialog.Builder(this);

            errorDialog.SetMessage(this.Resources.GetString(Resource.String.connection_problems));
            errorDialog.SetPositiveButton(Resource.String.try_again, delegate
            {
                RecieveFeferenceAsync(questID);
                errorDialog.Dispose();
            });
            errorDialog.SetNegativeButton(Resource.String.cancel, delegate
            {
                base.OnBackPressed();
            });

            helpDialogBuilder = new AlertDialog.Builder(this);
            helpDialogBuilder.SetMessage(this.Resources.GetString(Resource.String.capture_help));
            helpDialogBuilder.SetPositiveButton(Resource.String.open_camera, delegate
            {
                TakeAPicture();
            });
            helpDialogBuilder.SetCancelable(false);
            helpDialog = helpDialogBuilder.Create();

            cam = new CameraImplementation(this);
            api = QAAPI.GetInstance;

            Intent intent = this.Intent;
            questID = intent.GetStringExtra("questID");

            OpenCVImplementation.Initialize(this);

            Tools.RequestPermissions(this);

            InitializeLocationManager();

            if (!Tools.IsInternetOn())
            {
                errorDialog.Show();
            }

            if (cam.IsThereAnAppToTakePictures())
			{
				cam.CreateDirectoryForPictures();

                RecieveFeferenceAsync(questID);
                
            } else
            {
                DebugLog.LogFile.Print("No camera app found!");
            }
            //loadingDialog.Dismiss();
		}

        async Task RecieveFeferenceAsync(string id)
        {
            errorDialog = new AlertDialog.Builder(this);
            errorDialog.SetTitle(Resource.String.error);
            errorDialog.SetMessage(Resource.String.connection_error);
            errorDialog.SetPositiveButton(Resource.String.try_again, delegate {
                errorDialog.Dispose();
                RecieveFeferenceAsync(questID);
            });
            errorDialog.SetNegativeButton(Resource.String.cancel, delegate { base.OnBackPressed(); });

            var loadingHandler = new Handler();
            System.Action fail = () =>
            {
                errorDialog.Show();
                return;
            };

            System.Action success = async () =>
            {
                try
                {
                    referenceImage = ImageProcessing.GetImageFromUrl(this, api.Image);
                }
                catch (Exception ex)
                {
                    DebugLog.LogFile.Print(ex.Message);
                    errorDialog.Show();
                    return;
                }

                if (!pictureTaken)
                {
                    helpDialog.Show();
                }
                loadingDialog.SetMessage(Application.Context.GetString(Resource.String.loading_location));
            };

            api.SendDataComplete += () => { loadingHandler.Post(success); };

            api.SendDataFailed += () => { loadingHandler.Post(fail); };

            await api.GetReferences(id);
        }

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			if (requestCode == RequestLocationId)
			{
				AlertDialog.Builder dialog = new AlertDialog.Builder(this);

				if (grantResults[0] == Permission.Granted)
				{
					//Разрешения получены - инициализию геолокацию
					StartUpdatingLocation();
				}
				else
				{
					//Pазрешения не полученны - вывожу ошибку
					DebugLog.LogFile.Print("StagePage: Location and photo access denied.");

                    errorDialog.SetMessage(this.Resources.GetString(Resource.String.gps_off));
                    errorDialog.SetPositiveButton(Resource.String.turn_on, delegate
					{
						StartActivity(new Intent(Settings.ActionLocationSourceSettings));
					});
                    errorDialog.SetNegativeButton(Resource.String.cancel, delegate
                    {
                        base.OnBackPressed();
                    });
                    dialog.Show();
				}
			}
		}

		void TakeAPicture()
		{
            errorDialog = new AlertDialog.Builder(this);
            //В случае, если камера не доступна или еще что-то пошло не так ловлю исключения
            try 
			{
                pictureTaken = true;

				cam.SavePicture();

				StartActivityForResult(cam.Intent, 0);
			}
			catch (Exception e)
			{
				DebugLog.LogFile.Print("Error taking picture: " + e.Message);
                
                errorDialog.SetMessage(this.Resources.GetString(Resource.String.capture_error));
                errorDialog.SetPositiveButton(Resource.String.try_again, delegate
                {
                    TakeAPicture();
                    errorDialog.Dispose();
                });
                errorDialog.SetNegativeButton(Resource.String.cancel, delegate
                {
                    base.OnBackPressed();
                });
            }
		}

        void ReturnToPreviousActivity(int resID)
        {
            loadingDialog.Dismiss();

            //При ошибке делаю возврат на прошлое активити
            base.OnBackPressed();
        }
	
#region Location
		public void OnLocationChanged(Android.Locations.Location location)
		{
			Longitude = location.Longitude; Latitude = location.Latitude;

            var longDifference = Convert.ToDecimal(Math.Abs(Longitude - api.Longitude));
            var latDifferrence = Convert.ToDecimal(Math.Abs(Latitude - api.Latitude));

            if (longDifference > LOCATION_ACCURACY || latDifferrence > LOCATION_ACCURACY)
            {
                ReturnToPreviousActivity(1);
                return;
            }

            //if (!pictureTaken)
            //{
            //    helpDialog.Show();
            //}
        }

		public void OnProviderDisabled(string provider)
		{
			_locationManager.RemoveUpdates(this);
		}

		public void OnProviderEnabled(string provider)
		{
			
		}

		public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
		{
			
		}

		void InitializeLocationManager()
		{
			StartUpdatingLocation();
		}

		void UpdateLocationProvider()
		{
            AlertDialog.Builder gpsRequestDialogBuilder = new AlertDialog.Builder(this);
            gpsRequestDialogBuilder.SetMessage(this.Resources.GetString(Resource.String.gps_off));
            gpsRequestDialogBuilder.SetPositiveButton(Resource.String.turn_on, delegate
            {
                StartActivity(new Intent(Settings.ActionLocationSourceSettings));
            });
            gpsRequestDialog = gpsRequestDialogBuilder.Create();

            Criteria criteriaForLocationService = new Criteria
			{
				Accuracy = Accuracy.Fine
			};
			IList<string> acceptableLocationProviders = _locationManager.GetProviders(criteriaForLocationService, true);

			if (acceptableLocationProviders.Any())
			{
				_locationProvider = acceptableLocationProviders.First();
			}
			else
			{
				_locationProvider = string.Empty;
			}

			if (_locationProvider.Length < 1)
			{
				DebugLog.LogFile.Print("Location: Location service not enabled. Requsting it.");
                gpsRequestDialog.Show();
				return;
			}
		}

		void StartUpdatingLocation()
		{
			_locationManager = GetSystemService(Context.LocationService) as LocationManager;

			UpdateLocationProvider();

			DebugLog.LogFile.Print("Location provider: " + _locationProvider);

			//При неполадках с геолокацией пишу в лог
			try
			{
				_locationManager.RequestLocationUpdates(_locationProvider, 1000, 0, this);
			}
			catch (Exception e)
			{
				DebugLog.LogFile.Print("Location updates: " + e.Message);
			}
		}

		protected override void OnResume()
		{
			base.OnResume();

            gpsRequestDialog.Cancel();

            if (_locationManager != null)
				UpdateLocationProvider();

			if (!String.IsNullOrEmpty(_locationProvider))
				_locationManager?.RequestLocationUpdates(_locationProvider, 1000, 0, this);
		}

		protected override void OnPause()
		{
			base.OnPause();
			_locationManager?.RemoveUpdates(this);
		}

		async Task<Address> ReverseGeocodeCurrentLocationAsync()
		{
			Geocoder geocoder = new Geocoder(this);
			IList<Address> addressList =
				await geocoder.GetFromLocationAsync(Latitude, Longitude, 10);

			Address address = addressList.FirstOrDefault();
			return address;
		}

		async Task<string> GetAddressAsync()
		{
			var address = await ReverseGeocodeCurrentLocationAsync();

			if (address != null)
			{
				var adress = String.Format("{0} {1}", address.CountryCode, address.Locality); //Тестовое
				//gpsText.Text = String.Format("Lat: {0}, Long: {1}  {2}\n", Latitude, Longitude, adress); //Тестовое
				return String.Format("{0} {1}", address.CountryCode, address.Locality);
			}
			else
			{
				return null;
			}
		}
#endregion
	}	
}

