﻿
using Android.App;
using Android.OS;
using Android.Webkit;

namespace QuestArea
{
    [Activity(Label = "Privacy Policy")]
    public class PrivacyPolicy : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetTheme(Resource.Style.MainTheme);
            SetContentView(Resource.Layout.priavacy_policy);

            var webView = FindViewById<WebView>(Resource.Id.webView);

            var webClient = new WebViewClient();
            webView.SetWebViewClient(webClient);
            webView.LoadUrl("http://quest-area.com/privacy_policy");
        }
    }
}