﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using QuestAreaAPI;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Runtime;

namespace QuestArea
{
    [Activity(Label = "QuestArea",  ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, MainLauncher = true)]
	public class MainActivity : Activity
	{
		Activity activity;
		ProgressBar avatarProgressBar;
		TextView nickText;
        TextView currentFragmentText;
		ImageView smallAvatar;
		QAAPI api;
        SearchView searchQuestView;
        DrawerLayout drawer;
        NavigationView navView;
        Fragment fMain;
        Fragment fStory;
        Fragment fCreated;
        Fragment fFavourites;
        ISharedPreferences sharedPrefs;
        ProgressDialog questStartLoadingDialog;

        protected override void OnCreate(Bundle savedInstanceState)
		{
            SetTheme(Resource.Style.MainTheme);
			base.OnCreate(savedInstanceState);
            
			SetContentView(Resource.Layout.mainContainer);

            sharedPrefs = GetSharedPreferences("settings", FileCreationMode.Private);

            DebugLog.LogFile.CleanIfSize();
            Tools.RequestPermissions(this);

            var fragmentTx = FragmentManager.BeginTransaction();

            fMain = new FragmentMain();
            
            fragmentTx.Add(Resource.Id.rootLayout, fMain, "Current");
            fragmentTx.Commit();
                        
            avatarProgressBar = FindViewById<ProgressBar>(Resource.Id.avatarProgressBar);
            searchQuestView = FindViewById<SearchView>(Resource.Id.questSearch);
            drawer = FindViewById<DrawerLayout>(Resource.Id.navigationDrawer);
            navView = FindViewById<NavigationView>(Resource.Id.navView);
            currentFragmentText = FindViewById<TextView>(Resource.Id.toolBarText);

            currentFragmentText.Text = GetString(Resource.String.main_page);

            var header = navView.GetHeaderView(0);
            nickText = header.FindViewById<TextView>(Resource.Id.nickName);
            smallAvatar = header.FindViewById<ImageView>(Resource.Id.AvatarButton);

            Tools.SetCustomBackIfExist(this, sharedPrefs);
            
            header.FindViewById<LinearLayout>(Resource.Id.userNameLayout).Click += (e, sender) => {
                var token = Application.Context.GetSharedPreferences("API", FileCreationMode.Private).GetString("access_token", null);
                if (token == null)
                {
                    StartActivity(new Intent(this, typeof(signIn)));
                }
            };

            navView.NavigationItemSelected += (e, sender) => {
                var fragmentTransaction = FragmentManager.BeginTransaction();

                var authRequireDialog = new AlertDialog.Builder(this);
                authRequireDialog.SetMessage(Resources.GetString(QuestArea.Resource.String.login_required));
                authRequireDialog.SetPositiveButton("Ok", delegate
                {
                    StartActivity(new Intent(Application.Context, typeof(signIn)));
                });
                authRequireDialog.SetNegativeButton(Resource.String.cancel, delegate
                {
                    authRequireDialog.Dispose();
                });

                switch (sender.MenuItem.ItemId)
                {
                    case Resource.Id.nav_home:
                        {
                            fMain = new FragmentMain();
                            fragmentTransaction.SetTransition(FragmentTransit.EnterMask);
                            fragmentTransaction.Replace(Resource.Id.rootLayout, fMain, "Current");

                            RemoveAllEvents();
                            searchQuestView.QueryTextSubmit += MainSearchInvoke;
                            searchQuestView.QueryTextChange += MainUpdateInvoke;
                            currentFragmentText.Text = GetString(Resource.String.main_page);
                            break;
                        }
                    case Resource.Id.settingsMenu:
                        {
                            StartActivity(new Intent(this, typeof(profileSettings)));
                            break;
                        }
                    case Resource.Id.favouritesLK:
                        {
                            if (!Tools.IsLoggedIn)
                            {
                                authRequireDialog.Show();
                                return;
                            }

                            fFavourites = new FragmentFavourites();
                            fragmentTransaction.SetTransition(FragmentTransit.EnterMask);
                            fragmentTransaction.Replace(Resource.Id.rootLayout, fFavourites, "Current");

                            RemoveAllEvents();
                            searchQuestView.QueryTextSubmit += FavouritesSearchInvoke;
                            searchQuestView.QueryTextChange += FavouritesUpdateInvoke;
                            currentFragmentText.Text = GetString(Resource.String.favourite);
                            break;
                        }
                    case Resource.Id.historyLK:
                        {
                            if (!Tools.IsLoggedIn)
                            {
                                authRequireDialog.Show();
                                return;
                            }

                            fStory = new FragmentStory();
                            fragmentTransaction.SetTransition(FragmentTransit.EnterMask);
                            fragmentTransaction.Replace(Resource.Id.rootLayout, fStory, "Current");

                            RemoveAllEvents();
                            searchQuestView.QueryTextSubmit += HistorySearchInvoke;
                            searchQuestView.QueryTextChange += HistoryUpdateInvoke;
                            currentFragmentText.Text = GetString(Resource.String.history);
                            break;
                        }
                    case Resource.Id.CreatedQuestLK:
                        {
                            if (!Tools.IsLoggedIn)
                            {
                                authRequireDialog.Show();
                                return;
                            }
                                
                            fCreated = new FragmentCreated();
                            fragmentTransaction.SetTransition(FragmentTransit.EnterMask);
                            fragmentTransaction.Replace(Resource.Id.rootLayout, fCreated, "Current");

                            RemoveAllEvents();
                            searchQuestView.QueryTextSubmit += CreatedSearchInvoke;
                            searchQuestView.QueryTextChange += CreatedUpdateInvoke;
                            currentFragmentText.Text = GetString(Resource.String.created_quests);
                            break;
                        }
                    case Resource.Id.createQuestLK:
                        {
                            Android.Support.V7.App.AlertDialog.Builder dialog = new Android.Support.V7.App.AlertDialog.Builder(this);
                            dialog.SetMessage(Resources.GetString(QuestArea.Resource.String.create_not_available));
                            dialog.SetPositiveButton(Resources.GetString(QuestArea.Resource.String.openLink), delegate
                            {
                                var uri = Android.Net.Uri.Parse("https://quest-area.com");
                                var intent = new Intent(Intent.ActionView, uri);
                                StartActivity(intent);
                            });
                            dialog.SetNegativeButton(Resources.GetString(QuestArea.Resource.String.cancel), delegate { dialog.Dispose(); });
                            dialog.Show();
                            break;
                        }
                    case Resource.Id.privacyPolicy:
                        {
                            StartActivity(new Intent(this, typeof(PrivacyPolicy)));
                            break;
                        }

                    default:
                        break;
                }
                fragmentTransaction.AddToBackStack(null);
                fragmentTransaction.Commit();
                drawer.CloseDrawers();
            };

            api = QAAPI.GetInstance;
           
            SetDataFromCache();
            
			var loadingHandler = new Handler();
			System.Action fillUserInfo = OnSuccesUserRequest;
			System.Action showError = ShowConnectionError;
			activity = this;

            searchQuestView.QueryTextSubmit += MainSearchInvoke;
            searchQuestView.QueryTextChange += MainUpdateInvoke;
            searchQuestView.LayoutChange += (e, sender) =>
            {
                if (searchQuestView.Iconified)
                    currentFragmentText.Visibility = ViewStates.Visible;
                else
                    currentFragmentText.Visibility = ViewStates.Invisible;
            };

            api.SendDataComplete += () => {
                if (api.UserName != null)
                {
                    loadingHandler.Post(fillUserInfo);
                }
            };

            api.SendDataFailed += () =>
			{
				loadingHandler.Post(showError);
				return;
			};

            if (Intent.GetBooleanExtra("openDrawer", false))
            {
                //Открываю drawe с задержко в 500мс дабы пользователь увидел анимацию открытия
                var delayedHandler = new Handler();
                System.Action openDraweWithDelay = () =>
                {
                    OpenDrawer();

                    //TODO Анимашки
                    //var menuItem = navView.Menu.FindItem(Resource.Id.historyLK);
                    //menuItem.SetActionView();
                }; 

                delayedHandler.PostDelayed(openDraweWithDelay, 500);
                
            }
                
		}

        protected override void OnResume()
        {
            base.OnResume();

            Tools.SetCustomBackIfExist(this, sharedPrefs);

            if (api.Token != null)
                api.UpdateUserInfoAsync();
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            switch (keyCode)
            {
                case Keycode.Back:
                    {
                        var fragment = FragmentManager.FindFragmentByTag("Current");
                        var type = fragment.Class.SimpleName;

                        if (type == "FragmentMain")
                            CloseConfirmation();

                        break;
                    }
                default:
                    break;
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();

            var fragment= FragmentManager.FindFragmentByTag("Current");
            var type = fragment.Class.SimpleName;

            switch (type)
            {
                case "FragmentMain":
                    {
                        currentFragmentText.Text = GetString(Resource.String.main_page);
                        break;
                    }
                case "FragmentStory":
                    {
                        currentFragmentText.Text = GetString(Resource.String.history);
                        break;
                    }
                case "FragmentCreated":
                    {
                        currentFragmentText.Text = GetString(Resource.String.history);
                        break;
                    }
                case "FragmentFavourites":
                    {
                        currentFragmentText.Text = GetString(Resource.String.history);
                        break;
                    }
                default:
                    break;
            }
            return;
        }

        #region help methods
        void CloseConfirmation()
        {
            var dialogBuilder = new AlertDialog.Builder(this);

            dialogBuilder.SetMessage(Resources.GetString(QuestArea.Resource.String.exit_confirm));


            dialogBuilder.SetNegativeButton(Resource.String.cancel, delegate
            {
                dialogBuilder.Dispose();
            });

            dialogBuilder.SetPositiveButton("Ok", delegate
            {
                FinishAndRemoveTask();
            });

            dialogBuilder.Show();
        }
        #endregion
        #region Core methods
        void OnSuccesUserRequest()
		{
			ImageProcessing.SetRoundedImageViewAsync(ApplicationContext, api.AvatarUrl, smallAvatar, avatarProgressBar);
			nickText.Text = api.UserName;
        }

        void SetDataFromCache()
            {
            var sharedPrefs = GetSharedPreferences("API", FileCreationMode.Private);
            if (Tools.IsLoggedIn)
                nickText.Text = sharedPrefs.GetString("login", GetString(Resource.String.sign_in));
            else nickText.Text = GetString(Resource.String.sign_in);
        }

        void ShowConnectionError()
		{
            if (api.ErrorMessage != null)
                Toast.MakeText(this, api.ErrorMessage, ToastLength.Short).Show();
            Toast.MakeText(this, Resource.String.auth_error, ToastLength.Short).Show();

            Tools.ClearAuth();
            Tools.RestartAndOpenActivity(this, typeof(MainActivity));
            sharedPrefs
                .Edit()
                .Remove("login")
                .Commit();
        }

        void RemoveAllEvents()
        {
            searchQuestView.QueryTextSubmit -= FavouritesSearchInvoke;
            searchQuestView.QueryTextChange -= FavouritesUpdateInvoke;
            searchQuestView.QueryTextSubmit -= CreatedSearchInvoke;
            searchQuestView.QueryTextChange -= CreatedUpdateInvoke;
            searchQuestView.QueryTextSubmit -= HistorySearchInvoke;
            searchQuestView.QueryTextChange -= HistoryUpdateInvoke;
            searchQuestView.QueryTextSubmit -= MainSearchInvoke;
            searchQuestView.QueryTextChange -= MainUpdateInvoke;
        }
        #endregion
        #region SearchInvokes
        void MainSearchInvoke(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentMain>("Current");

            fragment.QuestSearch(e.Query);
        }

        void MainUpdateInvoke(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentMain>("Current");

            if (searchQuestView.Query.Length  < 1)
            {
                fragment.Update();
            }
        }

        void FavouritesSearchInvoke(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentFavourites>("Current");

            fragment.QuestSearch(e.Query);
        }

        void FavouritesUpdateInvoke(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentFavourites>("Current");

            if (searchQuestView.Query.Length < 1)
            {
                fragment.Update();
            }
        }

        void HistorySearchInvoke(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentStory>("Current");

            fragment.QuestSearch(e.Query);
        }

        void HistoryUpdateInvoke(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentStory>("Current");

            if (searchQuestView.Query.Length < 1)
            {
                fragment.Update();
            }
        }

        void CreatedSearchInvoke(object sender, SearchView.QueryTextSubmitEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentCreated>("Current");

            fragment.QuestSearch(e.Query);
        }

        void CreatedUpdateInvoke(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var fragment = FragmentManager.FindFragmentByTag<FragmentCreated>("Current");

            if (searchQuestView.Query.Length < 1)
            {
                fragment.Update();
            }
        }
        #endregion
        #region Public method section
        public void ShowQuestStartDialog(System.Action positiveButtonAction)
        {
            var dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.SetMessage(Resources.GetString(QuestArea.Resource.String.quest_start_confirm));

            dialogBuilder.SetPositiveButton("Ok", delegate
            {
                positiveButtonAction?.Invoke();

                questStartLoadingDialog = ProgressDialog.Show(this, GetString(Resource.String.loading_title), GetString(Resource.String.loading));
                //loadingDialog.SetCancelable(false);
            });
            
            dialogBuilder.Show();
        }

        public void HideQuestStartDialog()
        {
            questStartLoadingDialog.Dismiss();
        }

        public void CloseSearch()
        {
            if(CurrentFocus != null)
            {
                var inputManager = (Android.Views.InputMethods.InputMethodManager)GetSystemService(InputMethodService);
                inputManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, 0);
            }

            searchQuestView.ClearFocus();
        }

        public void OpenDrawer()
        {
            drawer.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
        }
        #endregion

        [Java.Interop.Export("OnMenuClick")]
        public void OnMenuClick(View v)
        {
            OpenDrawer();
        }
	}
}
