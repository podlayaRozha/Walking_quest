﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace QuestArea
{
    [Activity(Label = "Stages")]
    public class Stages : Activity
    {
        ListView stageList;
        QuestAreaAPI.QAAPI api;
        AlertDialog dialog;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetTheme(Resource.Style.MainTheme);
            SetContentView(Resource.Layout.Stages);

            var aleartBuilder = new AlertDialog.Builder(this);
            aleartBuilder.SetMessage(Resource.String.loading);
            aleartBuilder.SetCancelable(false);
            dialog = aleartBuilder.Create();
            dialog.Show();

            var questID = Intent.GetStringExtra("questID");
            api = QuestAreaAPI.QAAPI.GetInstance;
            stageList = FindViewById<ListView>(Resource.Id.stagesListView);

            stageList.ItemClick += (e, sender) =>
            {
                Intent intent = new Intent(Application.Context, typeof(Walkthrough));
                intent.PutExtra("questID", questID);
                StartActivity(intent);
            };

            var loadingHandler = new Handler();
            System.Action generateStages = OnSuccesQuestRequest;
            System.Action showError = OnFailRequest;

            api.SendDataComplete += () => loadingHandler.Post(generateStages);

            api.SendDataFailed += () => loadingHandler.Post(showError);
            api.GetStages(questID);
        }

        void OnSuccesQuestRequest()
        {
            var stageAdapter = new StageAdapter(this, api.Stages);
            stageList.Adapter = stageAdapter;
            dialog.Dismiss();
        }

        void OnFailRequest()
        {
            Toast.MakeText(this, "", ToastLength.Long).Show();
            base.OnBackPressed();
        }
    }
}