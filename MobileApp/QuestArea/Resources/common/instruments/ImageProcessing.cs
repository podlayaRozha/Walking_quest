﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using Android.Graphics;

namespace QuestAreaAPI
{
    static class ImageProcessing
	{
		static public async Task SetImageViewFromUrlAsync(Context ctx, string url, ImageView view, ProgressBar progressBar = null)
		{
            Bitmap result;

			using (WebClient webClient = new WebClient())
			{
				byte[] bytes;
				//Если нет подключения к интеренету, возвращаю null 
				try
				{
					bytes = await webClient.DownloadDataTaskAsync(url);
				}
				catch (Exception e)
				{
					DebugLog.LogFile.Print("ImageLoading" + e.Message);
					bytes = null;
				}
                
                if(bytes == null)
                {
                    result = BitmapFactory.DecodeResource(ctx.Resources, QuestArea.Resource.Drawable.img_loading_error);
                } else
                {
                    result = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);
                }
				view.SetImageBitmap(result);
			}
			if (progressBar != null)
				progressBar.Visibility = ViewStates.Gone;
		}

		static public void SetImageViewFromUrl(Context ctx, string url, ImageView view)
		{
            Bitmap result;

            using (WebClient webClient = new WebClient())
			{
				byte[] bytes;
				//Если нет подключения к интеренету, возвращаю null 
				try
				{
					bytes = webClient.DownloadData(url);
				}
				catch (Exception e)
				{
					DebugLog.LogFile.Print("ImageLoading" + e.Message);
					bytes = null;
				}

                if (bytes == null)
                {
                    result = BitmapFactory.DecodeResource(ctx.Resources, QuestArea.Resource.Drawable.img_loading_error);
                }
                else
                {
                    result = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                }
                view.SetImageBitmap(result);
			}
		}

		static public async Task<Bitmap> GetImageFromUrlAsync(Context ctx, string url)
		{
            Bitmap result;

            using (WebClient webClient = new WebClient())
			{
				byte[] bytes;
				//Если нет подключения к интеренету, возвращаю null 
				try
				{
					bytes = await webClient.DownloadDataTaskAsync(url);
				}
				catch (Exception e)
				{
					DebugLog.LogFile.Print("ImageLoading" + e.Message);
					bytes = null;
				}

                if (bytes == null)
                {
                    result = BitmapFactory.DecodeResource(ctx.Resources, QuestArea.Resource.Drawable.img_loading_error);
                }
                else
                {
                    result = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);
                }
                return result;
			}
		}

		static public Bitmap GetImageFromUrl(Context ctx, string url)
		{
            Bitmap result;

            using (WebClient webClient = new WebClient())
			{
				byte[] bytes;
				//Если нет подключения к интеренету, возвращаю null 
				try
				{
					bytes = webClient.DownloadData(url);
				}
				catch (Exception e)
				{
					DebugLog.LogFile.Print("ImageLoading" + e.Message);
					bytes = null;
				}

                if (bytes == null)
                {
                    result = BitmapFactory.DecodeResource(ctx.Resources, QuestArea.Resource.Drawable.img_loading_error);
                }
                else
                {
                    result = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
                }
                return result;
			}
		}

        static public async Task SetRoundedImageViewAsync(Context ctx, string url, ImageView view, ProgressBar progressBar = null)
        {
            Bitmap result;

            //Загрузка изображения
            using (WebClient webClient = new WebClient())
            {
                byte[] bytes;
                //Если нет подключения к интеренету, возвращаю null 
                try
                {
                    bytes = await webClient.DownloadDataTaskAsync(url);
                }
                catch (Exception e)
                {
                    DebugLog.LogFile.Print("ImageLoading" + e.Message);
                    bytes = null;
                }

                if (bytes == null)
                {
                    result = BitmapFactory.DecodeResource(ctx.Resources, QuestArea.Resource.Drawable.img_loading_error);
                }
                else
                {
                    result = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);
                }
            }

            //Преобразование изображения
            int targetWidth = view.Width;
            int targetHeight = view.Height;
            Bitmap targetBitmap = Bitmap.CreateBitmap(targetWidth,
                targetHeight, Bitmap.Config.Argb8888);

            Canvas canvas = new Canvas(targetBitmap);
            Android.Graphics.Path path = new Android.Graphics.Path();
            path.AddCircle(((float)targetWidth - 1) / 2,
                ((float)targetHeight - 1) / 2,
                (Math.Min(((float)targetWidth),
                    ((float)targetHeight)) / 2),
                Android.Graphics.Path.Direction.Ccw);

            canvas.ClipPath(path);
            Bitmap sourceBitmap = result;
            canvas.DrawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.Width,
                    sourceBitmap.Height),
                new Rect(0, 0, targetWidth, targetHeight), null);

            view.SetImageBitmap(targetBitmap);

            if (progressBar != null)
                progressBar.Visibility = ViewStates.Gone;
        }
    }
}