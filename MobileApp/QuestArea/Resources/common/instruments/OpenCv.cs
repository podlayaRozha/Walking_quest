﻿using System;
using System.Linq;
using Android.Graphics;
using OpenCV.Core;
using OpenCV.ImgProc;
using Java.Util;
using OpenCV.Android;
using Android.Content;

namespace QuestAreaTools
{

    public static class OpenCVImplementation
    {
        //Инициализация
        public static void Initialize(Context ctx)
        {
            if (!OpenCVLoader.InitDebug())
            {
                System.Console.WriteLine("OpenCV: Internal OpenCV library not found. Using OpenCV Manager for initialization");
				DebugLog.LogFile.Print("OpenCV: Internal OpenCV library not found. Using OpenCV Manager for initialization");
				OpenCVLoader.InitAsync(OpenCVLoader.OpencvVersion300, ctx, null);
            }
            else
            {
                System.Console.WriteLine("OpenCV: OpenCV library found inside package. Using it!");
				DebugLog.LogFile.Print("OpenCV: OpenCV library found inside package. Using it!");
            }
        }

        //Меняет размер битмапы из файла fileName
        public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            return resizedBitmap;
        }

        public static double compareImage(Bitmap img_1, Bitmap img_2)
        {
            //Если один из параметров прилетел null, кидаю exeption...И паникую
            if (img_1 == null || img_2 == null)
                throw new ArgumentNullException(img_1 == null? "img_1":"img_2", "None of parameters can be null.");

            Mat mat_1 = new Mat();
            Mat mat_2 = new Mat();

            Utils.BitmapToMat(img_1, mat_1);
            Utils.BitmapToMat(img_2, mat_2);

            Mat hist_1 = new Mat();
            Mat hist_2 = new Mat();

            MatOfFloat ranges = new MatOfFloat(0f, 256f);
            MatOfInt histSize = new MatOfInt(25);

            Imgproc.CvtColor(mat_1, mat_1, Imgproc.ColorRgb2gray);
            Imgproc.CvtColor(mat_2, mat_2, Imgproc.ColorRgb2gray);

            Imgproc.CalcHist(Arrays.AsList(mat_1).Cast<Mat>().ToList(), new MatOfInt(0),
                    new Mat(), hist_1, histSize, ranges);
            Imgproc.CalcHist(Arrays.AsList(mat_2).Cast<Mat>().ToList(), new MatOfInt(0),
                    new Mat(), hist_2, histSize, ranges);

            double res = Imgproc.CompareHist(hist_1, hist_2, Imgproc.CvCompCorrel);
            return res * 100;
        }

        public static Bitmap ConvertImageToBW(Bitmap SourceImg)
        {
            Mat MatToBW = new Mat();

            if (SourceImg != null)
            {
                Utils.BitmapToMat(SourceImg, MatToBW);
                //first we convert to grayscale
                Imgproc.CvtColor(MatToBW, MatToBW, Imgproc.ColorRgb2gray);
                //now converting to b/w
                Imgproc.Threshold(MatToBW, MatToBW, 0.5 * 255, 255, Imgproc.ThreshBinary);

                using (Bitmap newBitmap = Bitmap.CreateBitmap(MatToBW.Cols(), MatToBW.Rows(), Bitmap.Config.Argb8888))
                {
                    Utils.MatToBitmap(MatToBW, newBitmap);

                    return newBitmap;
                }
            }
            return null;
        }
    }
}
