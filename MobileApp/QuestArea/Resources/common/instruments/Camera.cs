﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using Java.IO;

namespace QuestArea
{

    public class CameraImplementation 
    {
		public File File {get; private set; }
		public File Directory { get; private set; }
		private Context _ctx;
		public Bitmap Bitmap;
		public Intent Intent { get; private set; }
        
        public CameraImplementation(Context ctx)
        {
            _ctx = ctx;
        }

		public void SavePicture()
		{
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); //Фикс на Android N
            StrictMode.SetVmPolicy(builder.Build());

            Intent = new Intent(MediaStore.ActionImageCapture);

			File = new File(Directory, String.Format("QuestArea_{0}.jpg", Guid.NewGuid()));

			Intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(File));
		}

		public void CreateDirectoryForPictures()
        {
            Directory = new File(
                Environment.GetExternalStoragePublicDirectory(
                    Environment.DirectoryPictures), "QuestArea");
            if (!Directory.Exists())
            {
                Directory.Mkdirs();
            }
        }

        public bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
                _ctx.PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }
    }
}