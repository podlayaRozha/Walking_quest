﻿using System;

using Android.App;
using Android.Content;
using Android.Net;
using Android;
using Android.Content.PM;
using Android.Graphics.Drawables;

namespace QuestAreaAPI
{
    static class Tools
	{
		public static bool IsInternetOn()
		{
			var connectivityManager = (ConnectivityManager)(Application.Context.GetSystemService(Context.ConnectivityService));
			NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
			return networkInfo != null && networkInfo.IsConnected;
		}

		public static void RestartAndOpenActivity(Context ctx, Type activity, bool openDrawer = false)
		{
			Intent intent = new Intent(ctx, activity);
			intent.AddFlags(ActivityFlags.ClearTop);
			intent.AddFlags(ActivityFlags.NewTask);
            intent.PutExtra("openDrawer", openDrawer);
			ctx.StartActivity(intent);
		}

        public static void RequestPermissions(Activity ctx)
        {
            //Если SDK ниже версии 23 - запрашивать разрешения в runtime не нужно, делаю возврат
            if (((int)Android.OS.Build.VERSION.SdkInt) < 23)
            {
                return;
            }

            string[] Permissions =
            {
                    Manifest.Permission.WriteExternalStorage,
                    Manifest.Permission.ReadExternalStorage,
                    Manifest.Permission.AccessCoarseLocation,
                    Manifest.Permission.AccessFineLocation,
                    Manifest.Permission.Camera
            };

            int numOfGrantedPermissions = 0;

            foreach (var item in Permissions)
            {
                if (ctx.CheckSelfPermission(item) == (int)Permission.Granted)
                {
                    numOfGrantedPermissions++;
                }
            }

            if (numOfGrantedPermissions != Permissions.Length)
            {
                var dialogBuilder = new AlertDialog.Builder(ctx);
                dialogBuilder.SetMessage(ctx.Resources.GetString(QuestArea.Resource.String.permissions_request));
                dialogBuilder.SetPositiveButton("Ok", delegate
                {
                    ctx.RequestPermissions(Permissions, 0);
                });
                dialogBuilder.Show();
            }
        }

        public static void SetCustomBackIfExist(Activity ctx, ISharedPreferences sharedPrefs)
        {
            if (sharedPrefs.GetString("backroundImage", null) != null)
            {
                var pathToImage = sharedPrefs.GetString("backroundImage", null);

                if(pathToImage == null)
                {
                    DebugLog.LogFile.Print("Custom backround: pathToImage is null");
                    return;
                }

                try
                {
                    Drawable drawable = Drawable.CreateFromPath(pathToImage);

                    ctx.Window.SetBackgroundDrawable(drawable);
                }
                catch (System.Exception e)
                {
                    DebugLog.LogFile.Print("Custom backround:" + e.Message);
                }
            }
            else
            {
                ctx.Window.SetBackgroundDrawableResource(QuestArea.Resource.Drawable.bg);
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                var sharedPrefs = Application.Context.GetSharedPreferences("API", FileCreationMode.Private);
                var token = sharedPrefs.GetString("access_token", null);

                return token == null ? false : true;
            }
        }

        public static void SaveAuth(string token)
        {
            var sharedPrefs = Application.Context.GetSharedPreferences("API", FileCreationMode.Private);
            sharedPrefs
                .Edit()
                .PutString("access_token", token)
                .Commit();
        }

        public static void ClearAuth()
        {
            var sharedPrefs = Application.Context.GetSharedPreferences("API", FileCreationMode.Private);
            sharedPrefs.Edit()
                .Remove("access_token")
                .Commit();
        }
    }
}