﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace DataStore
{
    static class Storage
    {
        private static string DIR_PATH = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/Storage/";
        private static string FILE_EXTENSION = ".json";

        public static void Store(string key, object obj)
        {
            Directory.CreateDirectory(DIR_PATH);
            string json = "";

            try
            {
                json = JsonConvert.SerializeObject(obj);
            }
            catch (Exception)
            {
            }

            try
            {
                var fullPath = DIR_PATH + key + FILE_EXTENSION;
                File.WriteAllText(fullPath, json);
            }
            catch (Exception)
            {
            }
        }

        public static object Get<T>(string key)
        {
            T result = default(T);
            string json = "";
            var fullPath = DIR_PATH + key + FILE_EXTENSION;

            try
            {
                json = File.ReadAllText(fullPath);
            }
            catch (Exception)
            {
            }

            try
            {
                result = JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
            }

            return result;
        }
    }
}