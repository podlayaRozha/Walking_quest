﻿using System;
using System.IO;

namespace DebugLog
{
    static class LogFile
	{
        private static string DIR_PATH = "sdcard/QuestArea";
        private static string FILE_PATH = "sdcard/QuestArea/log.txt";
        private const int MAX_FILE_SIZE = 25000; //25 килобайт

        public static void CleanIfSize()
		{
			FileInfo file = new FileInfo(FILE_PATH);
            
			if (!File.Exists(FILE_PATH))
				return;

			if (file.Length < MAX_FILE_SIZE)
				return;

			try
			{
				File.Delete(FILE_PATH);
			}
			catch (Exception)
			{
				return;
			}
		}

		public static void Print(String text)
		{
			Directory.CreateDirectory(DIR_PATH);

			try
			{
				File.AppendAllText(FILE_PATH, String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), text + System.Environment.NewLine));
			}
			catch (Exception e)
			{
				var t = e.Message;
			}
		}
	}
}