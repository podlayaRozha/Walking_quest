﻿using System.Text.RegularExpressions;
using QuestArea;

namespace QuestAreaAPI
{
    class Validator
    {
        private static readonly Regex PASSWORD_REGEX = new Regex(@"(?=.*\w)(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$");
        private static readonly Regex EMAIL_REGEX = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        
        public int ErrorStringID;

        public bool ValidateEmpty(string email = null, string password = null, string login = null)
        {
            ErrorStringID = Resource.String.empty_requered_filed;
            return email == "" | password == "" | login == "" ? false : true;
        }

        public bool ValidateAllData(string email, string password, string login)
        {
            if (email == "" || password == "" || login == "")
            {
                ErrorStringID = Resource.String.empty_requered_filed;
                return false;
            }

            if (!PASSWORD_REGEX.IsMatch(password))
            {
                ErrorStringID = Resource.String.wrong_passwod;
                return false;
            }

            if (!EMAIL_REGEX.IsMatch(email))
            {
                ErrorStringID = Resource.String.wrong_email;
                return false;
            }

            return true;
        }

        //public bool ValidateEmail(string email)
        //{
        //    if (email == "")
        //    {
        //        ErrorStringID = Resource.String.empty_requered_filed;
        //        return false;
        //    }

        //    if (!EMAIL_REGEX.IsMatch(email))
        //    {
        //        ErrorStringID = Resource.String.wrong_email;
        //        return false;
        //    }

        //    return true;
        //}

        //public bool ValidatePassword(string password)
        //{
        //    if (password == "")
        //    {
        //        ErrorStringID = Resource.String.empty_requered_filed;
        //        return false;
        //    }

        //    if (!PASSWORD_REGEX.IsMatch(password))
        //    {
        //        ErrorStringID = Resource.String.wrong_passwod;
        //        return false;
        //    }

        //    return true;
        //}
    }
}