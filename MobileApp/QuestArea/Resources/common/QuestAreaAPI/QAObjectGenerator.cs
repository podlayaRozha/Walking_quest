﻿using System.Linq;
using Newtonsoft.Json.Linq;
using QuestArea.Structures;

namespace QuestArea
{
    static class QAObjectGenerator
    {
        public static Quest[] GenerateQuestsFromJson(string json)
        {
            JObject JsonObj = JObject.Parse(json);
            var quests = new Quest[JsonObj.Count];

            for (int i = 0; i < JsonObj.Count; i++)
            {
                var key = "quest" + i;
                quests[i] = JsonObj[key].ToObject<Quest>();
            }
            return quests;
        }

        public static Stage[] GenerateStagesFromJson(string json)
        {
            JObject JsonObj = JObject.Parse(json);
            var stages = new Stage[JsonObj.Count];

            stages = JsonObj["stages"].ToObject<Stage[]>();

            return stages;
        }
    }
}