﻿using QuestAreaAPI;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using QuestAreaAPI;
using Newtonsoft.Json;
using QuestArea.Structures;

namespace QuestArea.Network
{
    static class Network
    {

        public static QAAPI.APIResponse BasicSendData(NameValueCollection param, string url, Action<Exception> errorCallback)
        {
            string rawResponse = "";
            QAAPI.APIResponse response = new QAAPI.APIResponse();
            try
            {
                var client = new WebClient();
                var byteResponse = client.UploadValues(url, param);
                rawResponse = Encoding.UTF8.GetString(byteResponse);
                response = JsonConvert.DeserializeObject<QAAPI.APIResponse>(rawResponse);
            }
            catch (Exception e)
            {
                errorCallback?.Invoke(e);
            }
            return response;
        }

        public static Stage[] StageSendData(NameValueCollection param, string url)
        {
            string rawResponse = "";
            Stage[] stages;

            try
            {
                var client = new WebClient();
                var byteResponse = client.UploadValues(url, param);
                rawResponse = Encoding.UTF8.GetString(byteResponse);
                stages = QAObjectGenerator.GenerateStagesFromJson(rawResponse);
                return stages;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static Quest[] QuestSendData(NameValueCollection param, string url)
        {
            string rawResponse = "";
            Quest[] quests;

            try
            {
                var client = new WebClient();
                var byteResponse = client.UploadValues(url, param);
                rawResponse = Encoding.UTF8.GetString(byteResponse);
                quests = QAObjectGenerator.GenerateQuestsFromJson(rawResponse);
                return quests;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}