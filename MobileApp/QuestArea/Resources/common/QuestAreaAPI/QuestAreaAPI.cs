﻿using System;
using System.Text;
using System.Net;
using Android.App;
using Android.Content;
using QuestArea;
using System.Collections.Specialized;
using Android.Widget;
using Newtonsoft.Json;
using Uri = System.Uri;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using QuestArea.Structures;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using QuestArea.Network;

namespace QuestAreaAPI
{
    public class QAAPI
    {
        public struct APIResponse
        {
            public bool status;
            public string error;
            public string access_token;
            public string login;
            public string email;
            public string avatar;
            public double longitude;
            public double latitude;
            public string image;
            public string timeSpent;

            public Dictionary<string, string> stages;
        }

        private static QAAPI _instance;
        private static readonly string ErrorTAG = "API error: ";
        private ISharedPreferences sharedPrefs;

        private QAAPI() { }

        public int UserID { get; private set; }
        public string ErrorMessage { get; private set; }
        public int ErrorStringID { get; protected set; }
        public string Token { get; private set; }
        public string Email { get; private set; }
        public string UserName { get; private set; }
        public string AvatarUrl { get; private set; }
        public string Image { get; private set; }
        public double Longitude { get; private set; }
        public double Latitude { get; private set; }
        public bool IsComplite { get; private set; }
        public string TimeSpent { get; private set; }

        public delegate void SendDataEventHandler();
        public event SendDataEventHandler SendDataComplete;
        public event SendDataEventHandler SendDataFailed;
        public ImageView image;
        public Quest[] Quests;
        public Stage[] Stages;

        private string _baseAPIUrl;
        private NameValueCollection _params;
        private APIResponse _response;

        public static QAAPI GetInstance
        {
            get
            {
                if (_instance != null)
                {
                    _instance = null;
                    _instance = new QAAPI();
                }

                _instance = new QAAPI();
                _instance.UserID = 0;
                _instance._baseAPIUrl = "https://quest-area.com/api";
                _instance.sharedPrefs = Application.Context.GetSharedPreferences("API", FileCreationMode.Private);
                _instance.Token = _instance.sharedPrefs.GetString("access_token", null);

                return _instance;
            }
        }

        private void AwakeConnectionError()
        {
            ErrorStringID = Resource.String.connection_problems;
            DebugLog.LogFile.Print(ErrorTAG + Application.Context.GetString(ErrorStringID));
            SendDataFailed?.Invoke();
        }

        private void BasicRequest(string requestUrl)
        {
            if (!Tools.IsInternetOn())
            {
                AwakeConnectionError();
                return;
            }

            Action<Exception> errorCallback = (e) => {
                ErrorStringID = Resource.String.other_error;
                ErrorMessage = Application.Context.GetString(Resource.String.other_error);
                DebugLog.LogFile.Print(ErrorTAG + e.Message);
                UpdateData();
                SendDataFailed?.Invoke();
            };

            _response = Network.BasicSendData(_params, requestUrl, errorCallback);

            //Если прилетела ошибка - сообщаю об этом
            if (!_response.status)
            {
                ErrorMessage = _response.error;
                DebugLog.LogFile.Print(ErrorTAG + _response.error);
                UpdateData();
                SendDataFailed?.Invoke();
                return;
            }
            else
            {
                UpdateData();
                SendDataComplete?.Invoke();
                return;
            }
        }

        private void QuestRequest(string requestUrl)
        {
            if (!Tools.IsInternetOn())
            {
                AwakeConnectionError();
                return;
            }
            
            try
            {
                Quests = Network.QuestSendData(_params, requestUrl);
            }
            catch (Exception e)
            {
                ErrorStringID = Resource.String.other_error;
                DebugLog.LogFile.Print(ErrorTAG + e.Message);
                SendDataFailed?.Invoke();
            }

            if (Quests != null)
                SendDataComplete?.Invoke();
        }

        private void StageRequest(string requestUrl)
        {
            if (!Tools.IsInternetOn())
            {
                AwakeConnectionError();
                return;
            }

            try
            {
                Stages = Network.StageSendData(_params, requestUrl);
            }
            catch (Exception e)
            {
                ErrorStringID = Resource.String.other_error;
                DebugLog.LogFile.Print(ErrorTAG + e.Message);
                SendDataFailed?.Invoke();
            }

            if (Stages != null)
                SendDataComplete?.Invoke();
        }

        public async Task RegisterAsync(string email, string login, string password)
        {
            _params = new NameValueCollection
            {
                ["email"] = email,
                ["login"] = login,
                ["password"] = password,
                ["password_again"] = password
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/signUp"));
        }

        public async Task LoginAsync(string login, string password)
        {
            _params = new NameValueCollection
            {
                ["email"] = login,
                ["password"] = password
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/signIn"));
        }

        private void UpdateUserInfo()
        {
            _params = new NameValueCollection
            {
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            BasicRequest(_baseAPIUrl + "/getUser");
        }

        public async Task UpdateUserInfoAsync()
        {
            _params = new NameValueCollection
            {
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/getUser"));
        }

        public async Task ChangeUserInfoAsync(string fieldToChage, string infoToChange)
        {
            _params = new NameValueCollection
            {
                ["token"] = Token,
                ["column"] = fieldToChage,
                ["update"] = infoToChange
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/update"));
        }

        public async Task AddToFavourites(int questID)
        {
            _params = new NameValueCollection
            {
                ["questID"] = questID.ToString(),
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/addFavourite"));
        }

        public async Task GetFeed()
        {
            _params = new NameValueCollection
            {
                ["token"] = Token == null ? "" : Token,
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => QuestRequest(_baseAPIUrl + "/QuestFeed"));
        }

        public async Task Search(string query)
        {
            _params = new NameValueCollection
            {
                ["token"] = Token == null ? "" : Token,
                ["column"] = "name",
                ["el"] = query
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => QuestRequest(_baseAPIUrl + "/search"));
        }

        public async Task GetReferences(string questID)
        {
            _params = new NameValueCollection
            {
                ["questID"] = questID,
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/getRefs"));
        }

        public async Task StartQuest(string questID)
        {
            _params = new NameValueCollection
            {
                ["questID"] = questID,
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/quest_start"));
        }

        public async Task CompliteQuest(string questID)
        {
            _params = new NameValueCollection
            {
                ["questID"] = questID,
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/CompliteQuest"));
        }

        public async Task RateQuest(string questID, float rate)
        {
            _params = new NameValueCollection
            {
                ["questID"] = questID,
                ["token"] = Token,
                ["rate"] = rate.ToString()
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => BasicRequest(_baseAPIUrl + "/RateQuest"));
        }

        public async Task GetStages(string questID)
        {
            _params = new NameValueCollection
            {
                ["questID"] = questID
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => StageRequest(_baseAPIUrl + "/getStage"));
        }

        public async Task GetStory()
        {
            _params = new NameValueCollection
            {
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => QuestRequest(_baseAPIUrl + "/getStory"));
        }

        public async Task GetFavourites()
        {
            _params = new NameValueCollection
            {
                ["token"] = Token
            };
            
            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => QuestRequest(_baseAPIUrl + "/getFavourites"));
        }

        public async Task GetCreated()
        {
            _params = new NameValueCollection
            {
                ["token"] = Token
            };

            //Ставлю в очередь задачу на отправку данных на сервер
            await Task.Run(() => QuestRequest(_baseAPIUrl + "/getCreated"));
        }

        private void UpdateData()
        {
            //Обновляю инфу в sharedPrefs
            if (_response.login != null)
                sharedPrefs.Edit().PutString("login", _response.login).Commit();

            Email = _response.email;

            UserName = _response.login == null
                ? sharedPrefs.GetString("login", Application.Context.GetString(Resource.String.sign_in))
                : _response.login;

            AvatarUrl = _response.avatar;
            Image = _response.image;
            Latitude = _response.latitude;
            Longitude = _response.longitude;
            TimeSpent = _response.timeSpent;

            Token = _response.access_token == null
                ? Token
                : _response.access_token;
        }
    }
}
