 $('button[name="moderation_list"]').click(function (event){
     $('section[name="moderation_conteiner"]').toggle('norml');
   })

   $('div[name="string_quest"]').click(function (event){
    $('div[name="created_quest"]').toggle('norml');
  })

  $.ajax({
    url: "https://quest-area.com/api/getFavourites",
    type: "POST",
    success: function(data) {
        let obj = jQuery.parseJSON(data);
        let count = Object.keys(obj).length;
        for(let i = 0; i < count; i++)
        {
            $('section[name="moderation_conteiner"]').append(`
            <div name="created_quest">
            <p>` + obj["quest" + i].description_quest + `</p>
            <p>Количество этапов:` + obj["quest" + i].number_of_stages + `</p>
            <div>
                <h4>` + obj["quest" + i].name_stage + `</h4>
            <p>` + obj["quest" + i].description_stage + `</p>

                <h5>Ответы:</h5>
                <p>Ответ по изображению</p>
                <img alt="Изображение для подтверждения" src=\"` + obj["quest" + i].img_stage + `"/>

                <p>Ответ по карте</p>
                <p> Координаты по ширине:` + obj["quest" + i].longitude + `</p>
                <p> Координаты по долготе:` + obj["quest" + i].latitude + `</p>

                <p>Ответ по тексту</p>
                <p id="name_quest"> ` + obj["quest" + i].answer + `</p>
                
        </div>

        <img alt="Изображение к квесту" src=\"` + obj["quest" + i].img_quest + `"/>
        
        <p>Время на прохождение:` + obj["quest" + i].time + `</p>

            <h4>Награда:</h4>

        <form action="#" method="POST">
        <input type="submit" value="Прошел" class="button_c" id="approve">
        <input type="button" value="Не прошел" class="button_c" id="not_approve">
        
        <div>
            <h4>Сообщение об ошибке</h4>
            <textarea placeholder="Причина, почему квест не прошел проверку" id="massage_error" cols="30" rows="10"></textarea>
            <input type="submit" value="Отправить" class="button_c" id="send">
        </div>
    </form>
        </div> 
            
            
            
            <figure id="elment_list">
            <img id="img_user" alt="Изображение к квесту" src=\"` + obj["quest" + i].image + `"/>
            <figcaption>
                <div>
                <p id="name_quest">` + obj["quest" + i].questname + `</p>
                <p id="rating">Рейтинг` + obj["quest" + i].rating + `</p>
            </div>
            </figcaption>
        </figure>`);
        }
    },
    fail: function(data){
        $("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
    }
  });
