$("#sUp").submit(function(event){
    event.preventDefault();
    $('.loading').css('display', 'block');
    $('.button_s').css('display', 'none');
    let $form = $(this);
    let serializedData = $form.serialize();
    $.ajax({
        url: "https://quest-area.com/api/signUp",
        type: "POST",
        data: serializedData,
        success: function(data) {
            $('.loading').css('display', 'none');
            $('.button_s').css('display', 'block');
              console.log(data);
            let obj = jQuery.parseJSON(data);
            if(obj.status == "true"){
                $("#sign_up_cont").hide();
                $(".sign_up").append('<div id="sign_up_cont1"><h4>Регистрация прошла успешно!</h4><a href="https://quest-area.com" class="button_s">Готово</a></div>');
                return;
            }
          
              if($('#error').is(':visible') )
            {
              $('#error').replace('<p id="error">'+obj.error+'</p>');
              return;
            }
            $("#sUp").append('<p id="error">'+obj.error+'</p>');   
        },
        fail: function(data){
            $('.loading').css('display', 'none');
            $('.button_s').css('display', 'block');
            $("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
        }
      });
    })
    
    
    $('#sIn').submit(function(event)
        {
            event.preventDefault()
            $('.loading').css('display', 'block');
            $('.button_s').css('display', 'none');
            let $form = $(this);
            let serializedData = $form.serialize();
            $.ajax(
                {
                    url:"https://quest-area.com/api/signIn" ,
                    type: "POST",
                    data: serializedData,
                    success: function(data){
                        $('.loading').css('display', 'none');
                        $('.button_s').css('display', 'block');
                        let obj = jQuery.parseJSON(data);
                        if(obj.status == "true")
                        {
                            $("#sign_in_cont").hide();
                            $(".sign_up").append('<div id="sign_up_cont1"><h4>Авторизация прошла успешно!</h4><a href="https://quest-area.com" class="button_s">Готово</a></div>');
                              return;
                        }
                      
                          if($('#error').is(':visible') )
                        {
                              $('#error').replace('<p id="error">'+obj.error+'</p>');
                              return;
                        }
                           $("#sign_in_cont").append('<p id="error">'+obj.error+'</p>');
                    },
                    fail: function(data){
                        $('.loading').css('display', 'none');
                        $('.button_s').css('display', 'block');
                        $("#sIn").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
                    }
                }
            );
        }
    )
    
    $("#button_sUp").click(function(){
        $("#sign_in_cont").hide();
        $("#sign_up_cont").show();
    })
    
    $("#button_back").click(function() {
        if( $('#sign_up_cont').is(':visible') ) 
        { 
            event.preventDefault();
            $("#sign_in_cont").show();
            $("#sign_up_cont").hide();
            return;
        }
    })
    