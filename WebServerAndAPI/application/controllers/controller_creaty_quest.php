<?php

class Controller_Creaty_Quest extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{	
		$auth = new Auth();
		$data = $auth->get_user_data();
		$data['url'] = $_SERVER["SERVER_NAME"];
		$data['header'] = 'Создание квеста';
		$this->view->generate('creaty_quest_view.php', 'template_view.php',$data);
	}
}