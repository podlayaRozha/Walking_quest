<?php

class Controller_Lk_Moder extends Controller
{
	public $model;
	public $view;

	function __construct()
	{
		$this->view = new View();
		$this->model = new Model_Lk_Moder();
		$this->post = $_POST;
	}

	function action_index()
	{	
		$auth = new Auth();
		$data = $auth->get_user_data();
		$data['url'] = $_SERVER["SERVER_NAME"];
		$data['header'] = 'Личный кабинет Модератора';
		$this->view->generate('lk_moder_view.php', 'templatelk_view.php',$data);
	}

	function action_getQuests()
	{
		if(!$this->model->getQuests()){
			$json['status'] = 'false';
			$json['error'] = 'нет соединения с бд';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$quest = $this->model->getQuests();

		for ($i=0; $i < count($quest); $i++) {
			if (!$this->model->getStages($quest[$i]['id_quest'])) {
				$stage[] = 'Этапа у этого квеста нет';
				continue;
			}
			$stage[] = $this->model->getStages($quest[$i]['id_quest']);
		}
		$json['status'] = 'true';
		for ($i=0; $i < count($quest); $i++) { 
			$json['quest'][$i] = $quest[$i];
			$json['stage'][$i] = $stage[$i];
		}
		echo(json_encode($json,JSON_UNESCAPED_UNICODE));
	}

	function action_upQuest()
	{
		$id_quest = $this->post['id_quest'];
		$approval = $this->post['approval'];
		$messege_moderator = $this->post['messege_moderator'];

		if ($this->model->upQuest($id_quest, $approval, $messege_moderator)) {
			$json['status'] = 'true';
		} else {
			$json['status'] = 'false';
			$json['error'] = 'Квест был одобрен ранее';
		}
		echo(json_encode($json,JSON_UNESCAPED_UNICODE));
		
	}
}