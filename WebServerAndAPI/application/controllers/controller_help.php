<?php

class Controller_help extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{	
		$data['url'] = $_SERVER["SERVER_NAME"];
		$this->view->generate('help_view.php', 'template_view.php');
	}
}