<?php

class Controller_Created extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{	
		$auth = new Auth();
		$data = $auth->get_user_data();
		$data['url'] = $_SERVER["SERVER_NAME"];
		$data['header']='<h1>QuestArea</h1>';
		$this->view->generate('created_view.php', 'template_view.php',$data);
	}
}