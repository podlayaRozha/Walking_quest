<?php

class Controller_Lk extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{	
		$auth = new Auth();
		$data = $auth->get_user_data();
		$data['url'] = $_SERVER["SERVER_NAME"];
		$data['header'] = 'Личный кабинет';
		$this->view->generate('lk_view.php', 'templatelk_view.php',$data);
	}

	
}