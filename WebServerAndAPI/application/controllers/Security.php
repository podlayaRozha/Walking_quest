<?php
trait Security {

	function checkXSS ($str) {


		return htmlentities($str);

	}

	function checkRegular( $str, $regx=NULL) {
		if (strlen($str)>0) {	
			if ($regx)
				return preg_match($regx, $str) ? $str : false;
			else return $str;
		}	
		else return false;
		

	}
	
}