<?php
require 'Security.php';

class Controller_Api 
{

	use Security;

	public $model;
	public $view;

	function __construct()
	{
		$this->view = new View();
		$this->model = new Model_APi();
		
		$this->post = $_POST;
		$this->file = $_FILES;
	}

	function action_createQuest()
	{
		$quest = explode(',', $this->post['arrQuest']);

		$name_quest = explode(':', $quest[0])[1] ;
		$name_quest = explode('"',$name_quest)[1];
		$name_quest = Security::checkXSS($name_quest);

		$description_quest = explode(':', $quest[2])[1];
		$description_quest = explode('"',$description_quest)[1];
		$description_quest = Security::checkXSS($description_quest);

		$number_of_stages = explode(':', $quest[3])[1];
		$number_of_stages = explode('"',$number_of_stages)[1];
		$number_of_stages = Security::checkXSS($number_of_stages);

		$time = explode(':', $quest[1])[1] ;
		$time = explode('"',$time)[1] * 10000;
		$time = Security::checkXSS($time);

		if (!Security::checkRegular($name_quest) || !Security::checkRegular($description_quest) || !Security::checkRegular($number_of_stages) || !Security::checkRegular($time)) 
		{
			$json['status'] = 'false';
			$json['error'] = 'Одно или несколько  полей пусты.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		if (empty($this->file)) {
			$json['status'] = "false";
			$json['error'] = "Изображение не отправлено";
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$img_quest = $this->file['imgFon'];

		if ($img = $this->model->verificathion_img($img_quest)) {
			$json['status'] = 'false';
			$json['error'] = $img;
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$imgDirQuest = 'https://quest-area.com/img/quests/';

		$name = time().$img_quest['name'];

		$src_quest="$imgDirQuest".$name;

		move_uploaded_file($img_quest['tmp_name'], $imgDirQuest);


		for ($i=1; $i < $number_of_stages+1; $i++) 
		{

			$stages[$i-1] = explode(':',$this->post["arrStage".$i]);

			$img_stages[$i-1] = $this->file["imgStage".$i];

			if ($img_stages[$i-1] != null) 
			{
			if ($img = $this->model->verificathion_img($img_stages[$i-1])) 
			{
			$json['status'] = 'false';
			$json['error'] = $img;
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
			}

			$imgDirStages = 'https://quest-area.com/img/stages/';

			$name = time().$img_stages[$i-1]['name'];

			$src_stages[$i-1] = "$imgDirStages".$name;

			move_uploaded_file($img_stages[$i-1]['tmp_name'], $imgDirStage);
			}
			else
			{
				$src_stages[$i-1] = 'https://quest-area.com/img/stages/default.png';
			}
		}

		for ($i=0; $i < $number_of_stages; $i++) 
		{ 
			$name_stages[$i] = explode('"', $stages[$i][1])[1];
			$stage[$i]['name_stage'] = $name_stages[$i];
		$stage[$i]['name_stage'] = Security::checkXSS($stage[$i]['name_stage']);

			$description_stage[$i] = explode('"', $stages[$i][2])[1];
			$stage[$i]['description_stage'] = $description_stage[$i];
		$stage[$i]['description_stage'] = Security::checkXSS($stage[$i]['description_stage']);

			$answer[$i] = explode('"', $stages[$i][3])[1];
			$stage[$i]['answer'] = $answer[$i];
		$stage[$i]['answer'] = Security::checkXSS($stage[$i]['answer']);

			$coordinates[$i]['lat'] = explode('"', $stages[$i][5])[0];
			$coordinates[$i]['lat'] = explode(',', $coordinates[$i]['lat'])[0];
		$stage[$i]['lat'] = Security::checkXSS($stage[$i]['lat']);

			$coordinates[$i]['lng'] = explode('"', $stages[$i][6])[0];
			$coordinates[$i]['lng'] = explode('}', $coordinates[$i]['lng'])[0];
		$stage[$i]['lng'] = Security::checkXSS($stage[$i]['lng']);

			$coordinates[$i] = implode(',', $coordinates[$i]);
			$stage[$i]['point'] = $coordinates[$i];
			if (!Security::checkRegular($stage[$i]['name_stage']) || !Security::checkRegular($stage[$i]['description_stage']))
			{
				$json['status'] = 'false';
				$json['error'] = 'Одно или несколько  полей пусты.';
				die(json_encode($json,JSON_UNESCAPED_UNICODE));
			}
		}
 
		$rewards = $_POST['arrAwards'];
		$rewards = explode(':', $rewards);

		$description_rewards = explode('"', $rewards[1])[1];
		$description_rewards = Security::checkXSS($description_rewards);

		$text_rewards = explode('"', $rewards[2])[1];
		$text_rewards = Security::checkXSS($text_rewards);

		$img_rewards = $this->file['imgAward'];
		if ($img_rewards != null) {
		
		if ($img = $this->model->verificathion_img($img_rewards)) {
			$json['status'] = 'false';
			$json['error'] = $img;
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$imgDirRewards = 'https://quest-area.com/img/rewards/';

		$name = time().$img_rewards['name'];

		$src_rewards="$imgDirRewards".$name;

		move_uploaded_file($img_rewards['tmp_name'], $imgDirStage);
		}
		else
		{
		$src_rewards = 'https://quest-area.com/img/rewards/default.png';
		}
		$user = $this->model->get_user($_SESSION['token']);
		$id_user = $user['id_user'];
		if($description_rewards == null && $text_rewards == null && $src_rewrads == null)
			$rewards['MAX(id_rewards)'] = 1;
		else
		$rewards = $this->model->set_rewards($description_rewards, $text_rewards, $src_rewards);

		if (!$rewards) {
			$json['status'] = 'false';
			$json['error'] = 'Нет подключения к бд';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		if ($this->model->creaty_quest($id_user, $name_quest, $description_quest, $number_of_stages, $src_quest, $time, $rewards['MAX(id_rewards)'])) {
			$id_quest = $this->model->get_id_quest($name_quest);
			for ($i=0; $i < $number_of_stages; $i++) 
			{ 

				if ($this->model->set_stage($stage[$i],$i, $id_quest['id_quest'], $src_stages[$i])) {
	 				$json['status'] = 'true';
				}
				else
				{
					$json['status'] = 'false';
					$json['error'] = 'неудача загрузки этапа №'.$i+1;
				}
			}	
		} else 
		{
	 		$json['status'] = 'false';
	 		$json['error'] = 'неудача загрузки квеста';
	 	}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}
	
	function action_compliteQuest()
	{
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$id_quest = $this->post['questID'];
		$stage = $this->model->get_stage($id_quest);
		$id_stage = $this->model->get_id_stage($stage['name']);
		$user = $this->model->get_user($token);
		$date_of_end = time();
        $date_of_begin = $this->model->get_date_of_begin($user['id_user']);
        $complite_time = $date_of_end - $date_of_begin["date_of_begin"];

		if($this->model->quest_end($id_stage,$date_of_end,$user['id_user']))
		{	
			$json['status'] = 'true';
		}
		else
		{
			$json['status'] = 'false';
		}
		echo(json_encode($json,JSON_UNESCAPED_UNICODE));
	}

	function action_getCreated()
	{
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$user = $this->model->get_user($token);
		$quests = $this->model->getCreated($user['id_user']);

		if(!$quests)
		{
			$json['status'] = 'false';
			$json['error'] = 'Созданного квеста нет';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		
		for ($i=0; $i < count($quests); $i++) { 
			$json["quest".$i]['id'] = $quests[$i]['id_quest'];
		    $json["quest".$i]['questname'] = $quests[$i]['name'];
				$creator = $this->model->creatorInfo($quests[$i]['id_user']);
		    $json["quest".$i]['creator'] = $creator['login'];
		    $json["quest".$i]['rating'] = $quests[$i]['rating'];
			$json["quest".$i]['creatorAvatar'] = $user['avatar'];
			$stage = $this->model->get_stage($quests[$i]['id_quest']);
				$json["quest".$i]['status'] = $this->model->get_status($user['id_user'],$stage['id_stage']);
				$json['quest'.$i]['image'] = $quests[$i]['img'];
				$json["quest".$i]['creatorAvatar'] = $creator['avatar'];
		    $json["quest".$i]['description'] = $quests[$i]['description'];
		}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_quest_start()
	{
		$id_quest = $this->post['questID'];
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$stage = $this->model->get_stage($id_quest);
		$id_stage = $this->model->get_id_stage($stage['name']);
		$creator = $this->model->creator($id_quest);
		$user = $this->model->get_user($token);
		if($creator['id_user'] === $user['id_user'])
		{
			$json['status'] = 'false';
			$json['error'] = 'Вы не можете пройти свой квест.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$date_of_begin = time();
		if($this->model->ins_passing($id_stage['id_stage'],$user['id_user'],$date_of_begin))
		{	
			$json['status'] = 'true';
		}
		else
		{
			$json['status'] = 'false';
			$json['error'] = 'Квест начат';
		}
		echo(json_encode($json,JSON_UNESCAPED_UNICODE));
	}

	function action_logout()
	{
		session_destroy();
	}

	function action_getStory()
	{
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$user = $this->model->get_user($token);
		$quests = $this->model->getStory($user['id_user']);

		if(!$quests)
		{
			$json['status'] = 'false';
			$json['error'] = 'История пуста';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		for ($i=0; $i < count($quests); $i++) { 
			$json["quest".$i]['id'] = $quests[$i]['id_quest'];
		    $json["quest".$i]['questname'] = $quests[$i]['name'];
			$creator = $this->model->creatorInfo($quests[$i]['id_user']);
		    $json["quest".$i]['creator'] = $creator['login'];
		    $json["quest".$i]['rating'] = $quests[$i]['rating'];
			$stage = $this->model->get_stage($quests[$i]['id_quest']);
			$json["quest".$i]['status'] = $this->model->get_status($user['id_user'],$stage['id_stage']);
				$json['quest'.$i]['image'] = $quests[$i]['img'];
			$json["quest".$i]['creatorAvatar'] = $creator['avatar'];
		    $json["quest".$i]['description'] = $quests[$i]['description'];
		}
		
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_search()
	{
		$column = Security::checkXSS($this->post['column']);
		$el = Security::checkXSS($this->post['el']);

		if (!Security::checkRegular($column) || !Security::checkRegular($el)) 
		{
			$json['status'] = 'false';
			$json['error'] = 'Одно или несколько  полей пусты.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$quests = $this->model->search($column, $el);

		if(!$quests)
		{
			$json['status'] = 'false';
			$json['error'] = 'Поиск не удался';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		for ($i=0; $i < count($quests); $i++) { 
			$json["quest".$i]['id'] = $quests[$i]['id_quest'];
		    $json["quest".$i]['questname'] = $quests[$i]['name'];
			$creator = $this->model->creatorInfo($quests[$i]['id_user']);
		    $json["quest".$i]['creator'] = $creator['login'];
		    $json["quest".$i]['rating'] = $quests[$i]['rating'];
			$stage = $this->model->get_stage($quests[$i]['id_quest']);
			$json["quest".$i]['status'] = $this->model->get_status($user['id_user'],$stage['id_stage']);
				$json['quest'.$i]['image'] = $quests[$i]['img'];
			$json["quest".$i]['creatorAvatar'] = $creator['avatar'];
		    $json["quest".$i]['description'] = $quests[$i]['description'];
		}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_getUser()
	{
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		if(!$this->model->get_user($token)){
			$json['status'] = 'false';
			$json['error'] = 'неверная авторизация';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$user = $this->model->get_user($token);
		$json['status'] = 'true';
		$json['login'] = $user['login'];
		$json['avatar'] = $user['avatar'];
		echo(json_encode($json,JSON_UNESCAPED_UNICODE));

	}

	function action_questFeed()
	{	
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$user = $this->model->get_user($token);
		$quests = $this->model->questFeed();
		for ($i=0; $i < 5; $i++) { 
			if (empty($quests[$i]['id_quest'])) continue;
				$json["quest".$i]['id'] = $quests[$i]['id_quest'];
				$json["quest".$i]['questname'] = $quests[$i]['name'];
				$creator = $this->model->creatorInfo($quests[$i]['id_user']);
				$json["quest".$i]['creator'] = $creator['login'];
				$json["quest".$i]['rating'] = $quests[$i]['rating'];
				$stage = $this->model->get_stage($quests[$i]['id_quest']);
				$json["quest".$i]['status'] = $this->model->get_status($user['id_user'],$stage['id_stage']);
				$json['quest'.$i]['image'] = $quests[$i]['img'];
				$json["quest".$i]['creatorAvatar'] = $creator['avatar'];
				$json["quest".$i]['description'] = $quests[$i]['description'];
			}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_getFavourites()
	{
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$user = $this->model->get_user($token);
		$quests = $this->model->getFavourites($user['id_user']);
		if(!$quests)
		{
			$json['status'] = 'false';
			$json['error'] = 'Избранного квеста нет';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}


		for ($i=0; $i < count($quests); $i++) { 
			$json["quest".$i]['id'] = $quests[$i]['id_quest'];
		    $json["quest".$i]['questname'] = $quests[$i]['name'];
				$creator = $this->model->creatorInfo($quests[$i]['id_user']);
		    $json["quest".$i]['creator'] = $creator['login'];
		    $json["quest".$i]['rating'] = $quests[$i]['rating'];
			$json["quest".$i]['creatorAvatar'] = $user['avatar'];
			$stage = $this->model->get_stage($quests[$i]['id_quest']);
				$json["quest".$i]['status'] = $this->model->get_status($user['id_user'],$stage['id_stage']);
				$json["quest".$i]['creatorAvatar'] = $creator['avatar'];
				$json['quest'.$i]['image'] = $quests[$i]['img'];
		    $json["quest".$i]['description'] = $quests[$i]['description'];

		}

		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_signIn()
	{	
		
		$email = Security::checkXSS($this->post['email']);
		$password = Security::checkXSS($this->post['password']);
		$login = 'login';

		if (!Security::checkRegular($email) || !Security::checkRegular($password) || !Security::checkRegular($login)) 
		{
			$json['status'] = 'false';
			$json['error'] = 'Одно или несколько  полей пусты.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		if(!$this->model->verificathion($email,$login,$password))
		{
			$json['status'] = 'false';
			$json['error'] = 'Пароль или Email неправильны.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$token = $this->model->token_user($email);

		$this->model->up_token($email,$token);

		if (!$this->model->sign_in($email,$password)) {
			$json['status'] = 'false';
			$json['error'] = 'Email или пароль не совподают.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$json['status'] = 'true';
		$json['access_token'] = $token;

		$_SESSION['token']=$token;
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_signUp()
	{
		$email = Security::checkXSS($this->post['email']);
		$password = Security::checkXSS($this->post['password']);
		$login = Security::checkXSS($this->post['login']);
		$password_again = Security::checkXSS($this->post['password_again']);

		if (!Security::checkRegular($email) || !Security::checkRegular($password) || !Security::checkRegular($password_again) || !Security::checkRegular($login)) 
		{
			$json['status'] = 'false';
			$json['error'] = 'Одно или несколько  полей пусты.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		if ($data = $this->model->existence($email, $login)) {
			$json['status'] = 'false';
			$json['error'] = $data.' уже занят.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		if ($password != $password_again) {
			$json['status'] = 'false';
			$json['error'] = 'Пароли не совпадают.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		if (!$this->model->verificathion($email, $login, $password)) {
			$json['status'] = 'false';
			$json['error'] = 'Данные введены не коректно.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$token = $this->model->token_guest();

		if (!$this->model->sign_up($email, $login, $password,$token)) {
			$json['status'] = 'false';
			$json['error'] = 'Ошибка подключеня к бд.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$json['status'] = 'true';
		$json['access_token'] = $token;

		$_SESSION['token']=$token;
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_update()
	{
		$token =  Security::checkXSS($this->post['token']); 
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$column = Security::checkXSS($this->post['column']);
		$update = Security::checkXSS($this->post['update']);

		if (!Security::checkRegular($column) || !Security::checkRegular($update)) 
		{
			$json['status'] = 'false';
			$json['error'] = 'Одно или несколько  полей пусты.';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		if ($this->model->update($token, $column, $update)) 
		{
			$json['status'] = 'true';
		}
		else
		{
			$json['status'] = 'false';
			$json['error'] = 'Неверные данные';
		}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_uppas()
	{
		$token =  Security::checkXSS($this->post['token']);

		if (empty($token)) {
			$token = $_SESSION['token'];
		}

		$update =  Security::checkXSS($this->post['update']);
		$password = $this->model->selpas($token);
		if($update == $password)
		{
			$json['status'] = 'false';
			$json['error'] = 'вы не можете изменить пароль на старый';
			die(json_encode($json));
		}
		if ($this->model->update($token,'password',$password)) {
			$json['status'] = 'true';
		}
		else
		{
			$json['status'] = 'false';
			$json['error'] = 'invalid data';
		}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	/*function action_rateQuest()
	{
		$id_quest = $this->post["questID"];
        $rate = $this->post["rate"];

        $rating = $this->model->getRate($id_quest);

        $rate = $rate+$rating;

        if(!$this->model->rateQuest($id_quest, $rate,))
        {
        	$json['status'] = 'false';
			$json['error'] = 'invalid id_quest';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
        }
		$json['status'] = 'true';
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}*/

	function action_getRefs()
	{
		$id_quest = $this->post['questID'];
		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$quest = $this->model->getRefs($id_quest);

		if (!$quest) {
			$json['status'] = 'false';
			$json['error'] = 'invalid id quest';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$point = explode( ',', $quest['point']);
		$longitude = $point[0];
		$latitude = $point[1];

		$json['status'] = 'true';
		$json['image'] = $quest['img'];
		$json['longitude'] = $longitude;
		$json['latitude'] = $latitude;
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}
	
	function action_addFavourite()
	{
		$id_quest = $this->post['questID'];

		$token = $this->post['token'];
		if (empty($token)) {
			$token = $_SESSION['token'];
		}
		$user = $this->model->get_user($token);

		$stage = $this->model->get_stage($id_quest);

		if (!$this->model->addFavourite($user['id_user'],$stage['id_stage'])) {
			$json['status'] = 'false';
			$json['error'] = 'неверная авторизация';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$json['status'] = 'true';
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_getStage()
	{
		$id_quest = $this->post['questID'];
		$stage = $this->model->get_stage($id_quest);
		if($stage)
		{
			$json["stages"][0]['title'] = $stage['name'];
			$json["stages"][0]['description'] = $stage['description'];
		}
		else
		{
			$json['status'] = 'false';
			$json['error'] = 'У квеста нет этапа';
		}
		echo json_encode($json,JSON_UNESCAPED_UNICODE);
	}

	function action_up_img()
	{
		$token =  Security::checkXSS($this->post['token']);
		if (empty($token)) {
			$token = $_SESSION['token'];
		}

		if (empty($this->file)) {
			$json['status'] = "false";
			$json['error'] = "Изображение не отправлено";
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$img = $this->file;

		if ($img = $this->model->verificathion_img($img)) {
			$json['status'] = 'false';
			$json['error'] = $img;
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}
		$user = $this->model->get_user($token);

		$imgDir = 'https://quest-area.com/img/users/';

		$name = time().$_FILES['img']['name'];

		$src="$imgDir".$name;

		if (!$this->model->up_img($src,$img,$user['id_user'])) {
			$json['status'] = 'false';
			$json['error'] = 'неверная авторизация';
			die(json_encode($json,JSON_UNESCAPED_UNICODE));
		}

		$json['status'] = 'true';
		echo json_encode($json,JSON_UNESCAPED_UNICODE);		
	}
}
