<?php


class Controller_Test extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{
		$auth = new Auth();
		$data = $auth->get_user_data();
		$data['header']='Test';
		$this->view->generate('test_view.php', 'template_view.php',$data);
	}
}