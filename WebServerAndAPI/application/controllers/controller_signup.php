<?php

class Controller_Signup extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{	
		$auth = new Auth();
		$data = $auth->get_user_data();
		$data['url'] = $_SERVER["SERVER_NAME"];
		$this->view->generate('signup_view.php', 'templatesign_view.php',$data);
	}
}