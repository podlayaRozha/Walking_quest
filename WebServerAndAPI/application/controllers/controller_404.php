<?php

class Controller_404 extends Controller
{
	function __construct()
	{
		$this->view = new View();
	}

	function action_index()
	{	
		$data['url'] = $_SERVER["SERVER_NAME"];
		$data['title'] = 'Ошибка 404';
		$this->view->generate('404_view.php', 'templatesign_view.php');
	}
}