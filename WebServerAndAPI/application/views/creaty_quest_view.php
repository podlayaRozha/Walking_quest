    <link rel="stylesheet"  href="css/creaty-quest.css">
</head>
<body> 
    <section class="creaty_q">
            
            <div class="creat_z" name="creat_form">
                <input type="text" placeholder="Название квеста" name="name_quest" size="40">
                <textarea placeholder="Описание квеста" name="description_quest" cols="40" rows="10"></textarea>
            <input type="number" placeholder="Количество этапов:" name="num_of_stages" min="1">
            <input type="button" class="button_c" value="ОК" name="confirmations">
        
            <hr>

      <input class="button_c" type="button" value="Этап №" name="stage">
      <div id="stage_conteiner" class="lol">

      <input type="text" placeholder="Название этапа" name="name_stage">
      <textarea placeholder="Описание этапа" name="description_stage" cols="30" rows="10"></textarea>
      
          <input type="button" class="button_c" value="Ответы" name="answers"><!--!При нажатии разворачивоется поле с checkbox-ми-->
        <div id="answer_conteiner">
          <!--При выборе одного или нескольких checkbox-ов разворачивается соответствующее ему поле-->
            <input type="checkbox" name="image" id="image_l">
            <label for="image_l" class="disclose">Изображение</label>
            <div id="img_ansver_conteiner"> 
                    <div class="upload_form">
                            <label>
                                <input type="file" name="stage_img" class="main_input_file" accept="image/jpeg,image/png">  <!--Добавить изображение-->
                                <div>Загрузить изображение</div>
                                <input class="f_name" type="text" id="s_name" value="Файл не выбран." disabled>
                            </label>
                        </div>
 
            </div>
            <input type="checkbox" name="maps" id="maps_l">
            <label for="maps_l" class="disclose">Координаты</label>
        
            <div id="map_ansver_conteiner">
                <div id="map">
                    <p id="coordinates"></p>
                </div>
            </div>

            <input type="checkbox" name="text" id="text_l">
            <label for="text_l" class="disclose">Текст</label>
        
            <div id="text_ansver_conteiner">
                <textarea placeholder="Текст" name="answer" cols="30" rows="10"></textarea>
            </div>
        </div>
        <button class="button_c" name="next_stage">Следующий этап</button>
        
        </div>
                <hr> 
      
            

            <div class="upload_form">
                    <h4>Фоновое изображение квеста</h4>
                    <label>
                        <input type="file" id="f_img_l" name="f_img" class="main_input_file" accept="image/jpeg,image/png">  <!--Фоновое изображение квеста-->
                        <div>Загрузить изображение</div>
                        <input class="f_name" type="text" id="f_name" value="Файл не выбран." disabled>
                    </label>
                </div>


                <hr>
            <input type="number" placeholder="Время на прохождение" name="time_max" min="1" max="24">

                    <input type="button" class="button_c" value="Награды" name="rewards"> <!--При нажатии на кнопку раскрывается список с Наградами в виде checkbox-ов-->
 
                    <div id="rewards_conteiner">
                    <textarea placeholder="Описание награды" name="description_rewrds" cols="30" rows="10"></textarea>
                
                <!--По нажатию одного из checkbox-ов раскрывается соответсвующее ему поле-->
                <input type="checkbox" name="r_link" id="r_link_l">
                <label for="r_link_l" class="disclose">Ачивка</label>
                <div id="achievement_link">
                <a href="#" name="rewards_link">Ачивка</a>
                </div>
                    <input type="checkbox" name="r_text" id="r_text_l">
                    <label for="r_text_l" class="disclose">Текстовое сообщение</label>
                    <div id="achievement_text">
                        <textarea placeholder="Текст" name="rewards_text" cols="30" rows="10"></textarea>
                    </div>
                    
                    <input type="checkbox" name="r_img" id="r_img_l">                    
                    <label for="r_img_l" class="disclose">Изображение</label>
                    <div id="achievement_img">
                            <div class="upload_form">
                                    <label>
                                        <input type="file" name="rewards_img" class="main_input_file" accept="image/jpeg,image/png">
                                        <div>Загрузить изображение</div>
                                        <input class="f_name" type="text" id="a_name" value="Файл не выбран." disabled>
                                    </label>
                                </div>
               
                    </div>   
            </div>
            
            <input type="submit" value="Создать" class="button_c" name="submit">
            </div>
                

            </section>
            
        <script src="js/general.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsuZrTOl8eQXjJWq1jl0mKRoRM9a2PbuY&callback=initMap"
        async defer></script>
        <script src="js/creaty-quest.js"></script>