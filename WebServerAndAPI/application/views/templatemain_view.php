<!DOCTYPE html>
    <html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QuestArea</title>
    <link rel="stylesheet"  href="css/normalize.css">
    <link rel="stylesheet"  href="css/general.css">
    <link rel="stylesheet"  href="css/style.css">
    <link rel="stylesheet" href="css_i/fontawesome-all.css">
    <link rel="shortcut icon" href="img/logot3.png"> 
    <script src="js/jquery-3.3.1.min.js"></script>
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><img src="img/logot4.png"></li>
                <li><a href="main">Главная</a></li>
                <?php
                    if(isset($_SESSION['token']))
                    {
                ?>
                <li><a href="creaty_quest">Создать квест</a></li>
                   
                <li>
                    <div class="art">
                        <?if($data['id_type'] == 1){?>
                            <a href="lk"><?= $data['login']?></a>
                        <?}else{?>
                            <a href="lk_moder"><?= $data['login']?></a>
                        <?}?>
                            <div class="ava">
                                <img src="<?= $data['avatar']?>" alt="Аватарка">
                            </div>
                    </div>
                </li>
                <?php
                    }
                    else
                    {
                ?>
                <li><a href="signup" id="accordion">Авторизация</a></li>
                <?php }?>
            </ul>
        </nav>

        <div class="q_area">
                    <h1>QuestArea</h1>  
                    <p>Окружающий наш мир кажется, довольно серым и обыденным, лишь иногда мы можем позволить себе красачные события, которые состовляют подовляющее большенство приятных нам воспоминаний.
                        Используя существующие технологии можно создать большое колличество интересных задач, которые позволяют объединить людей в рамках общих интересов и тем самым развивая коммуникацию в
                        Данный проект, включает в себя мобильное приложение и сайт, мы даем вам возможность взять на себя роль того кто проходит и того кто создает квесты.
                    </p>
        </header>

	<?php include 'application/views/'.$content_view; ?>

 <footer>
        <div class="footer_r">
            <div class="footer_cont"> 
                <div class="soc">   
                    <h4>О нас:</h4>
            <ul>
                <li><a href="https://vk.com/" class="fab fa-vk fa-lg"></a></li>
                <li><a href="https://www.facebook.com/" class="fab fa-facebook-f fa-lg"></a></li>
                <li><a href="https://twitter.com/" class="fab fa-twitter fa-lg"></a></li>
            </ul>
        </div>
            <ul>
                    <li><a href="privacy_policy">Политика конфиденциальности</a></li>
                </ul>
            </div>
        </div>
        </footer>
        <script src="js/main.js"></script>
</body>
</html>