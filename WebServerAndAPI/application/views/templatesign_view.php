<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QuestArea</title>
    <link rel="stylesheet"  href="css/normalize.css">
    <link rel="stylesheet"  href="css/sign-up.css">
    <link rel="shortcut icon" href="img/logot3.png">
    <script src="js/jquery-3.3.1.min.js"></script>      
    </head>
<body>

	<?php include 'application/views/'.$content_view; ?>

</body>
</html>