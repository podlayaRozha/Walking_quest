
<body>                       
   <section class="creaty_quest">
        <h2>Создание квестов</h2>
            <p>
            Особенностью данного проекта является площадка по созданию квестов, именно здесь вы находитесь на данный момент.
            Приступить к созданию квестов вы можете бесплатно, все что нужно так это зарегестрироваться на сайте или мобильном приложении. 
            Создавать квесты, вы можете где угодно и когда угодно, имея под рукой мобильное устройство или компьютер с выходом в интернет. Удобно, неправда ли?
            Созданные вами квесты, в дольнейшем будут проходить и оценивать другие пользователи. 
            </p>
            <button>Создать квест</button>
    </section>
    <section class="slider">
        <div class="slideshow-container">
            <figure class="mySlides fade slayd1">
            </figure>
            <figure class="mySlides fade slayd2">
            </figure>
            <figure class="mySlides fade slayd3">
            </figure>
            <div class="group_dot">
                    <span class="dot" onclick="currentSlide(1)"></span> 
                    <span class="dot" onclick="currentSlide(2)"></span> 
                    <span class="dot" onclick="currentSlide(3)"></span> 
                  </div>
        </div>
       
    </section>
    <section class="play_quest">
        <h2>Мобильное приложение "QuestArea"</h2>
        <p>
            <!-- Мобильное приложение "QuestArea" - это плащадка на которой вы можете проходят и оценивают квесты других людей! -->
        Так как есть площадка по созданию квестов, то должна быть и площадка по их прохождению.
        Для этого было созданно мобильное приложение "QuestArea", именно здесь вы сможете проходить квесты других людей и именно
        здесь люди будут проходить ваши квесты, при этом находясь где угодно!
        Скачать данное приложение в Google Play вы можете нажав на кнопку ниже.
        </p>
        <a href=""><img src="img/google-play-badge.png" alt="Ссылка на GooglePlay"></a>
    </section>