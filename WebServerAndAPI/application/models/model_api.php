<?php

class Model_Api extends Model
{
	public function get_stage($id_quest)
	{
		$number = 1;
		if (!($preparedStatement = $this->db->prepare('SELECT * FROM stage WHERE id_quest= ? and `number` = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("si", $id_quest, $number);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$stage = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $stage;
		}
		
	}

	public function get_user($token)
	{
		if (!($preparedStatement = $this->db->prepare('SELECT * FROM user WHERE token = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("s", $token);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$data = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $data;
		}
		
	}

	public function get_user_id($id_user)
	{
		if (!($preparedStatement = $this->db->prepare('SELECT * FROM user WHERE id_user = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("s", $id_user);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$data = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $data;
		}
	}

	 public function verificathion($email,$login,$password)
	 {
	 	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	 		return false;
	 	}

	 	if (!preg_match("#^[a-zA-Z0-9]+$#i", $login)) {
	 		return false;
	 	}

	 	if (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", $password)) {
	 		return false;
	 	}

	 	return true;
	 }

	 public function existence($email, $login)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM user WHERE email= ? or login = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("ss", $email, $login);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$data[] = $row;
			}

			for ($i=0; $i < count($data); $i++) 
			{ 
				if($data[$i]['email'] == $email)
			{
				return $data[$i]['email'];
			}
			
			}

			for ($i=0; $i < count($data); $i++) 
			{ 
				if($data[$i]['login'] == $login)
			{
				return $data[$i]['login'];
			}
			
			}
			return false;
		}
	 	
	 }

	 public function token_guest()
	 {
	 	$query = "SELECT MAX(id_user) FROM user";
		$result = mysqli_query($this->db, $query);
		$date = $result->fetch_assoc();
		$id = ++$date['MAX(id_user)'];
		$time = date("d:m:Y/h:i:s A");
		$hash = md5(md5($id)).md5(md5($time));
		return $hash;
	 }

	 public function token_user($email)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT id_user FROM user WHERE email = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("s", $email);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$id = $result->fetch_array(MYSQLI_ASSOC)['id_user'];
			$time = date("d:m:Y/h:i:s A");
			$hash = md5(md5($id)).md5(md5($time));
			$preparedStatement->close();
			return $hash;
		}
		
	 }

	 public function sign_up($email, $login, $password,$token)
	 {
	 	$uploaddir = "https://quest-area.com/img/users/default.png";
	 	$id_type = 1; 

	 	if (!($preparedStatement = $this->db->prepare('INSERT into user(login,password,email,id_type,token, avatar) VALUES (?,?,?,?,?,?);'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("sssiss",$login,  $password, $email, $id_type, $token, $uploaddir);
        $preparedStatement->execute();
        
        $result=$preparedStatement->affected_rows;  
         

		if (!$result) 
			return false; 
		else 
		{
			$preparedStatement->close();
			return true;
		}
	 }

	 public function get_date_of_begin($id)
  	 {
  	 	if (!($preparedStatement = $this->db->prepare('SELECT `date_of_begin` FROM `passing` WHERE id_user =?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("i",$id);
        $preparedStatement->execute();
        
        $result=$preparedStatement->get_result();  
         

		if (!$result) 
			return false; 
		else 
		{
			$preparedStatement->close();
			return $result->fetch_array(MYSQLI_ASSOC);
		}

  	 }

	 public function creaty_quest($id_user, $nameQuest, $descriptionQuest, $numberOfStages, $imgQuest, $time, $id_rewards)
	 {
	 	//move_uploaded_file($img['tmp_name'], $imgDirQuest);
	 	$date_of_create = (string) date("d:m:Y/h:i:s A");
	 	$approval = 0;
	 	$id_type = 1;
	 	$rating = 0;
	 	$id_city = 1;
	 	if (!($preparedStatement = $this->db->prepare('INSERT into quests(id_user,name,date_of_create,approval,description, id_type, rating, id_city, img, numder_of_stages,id_rewards, `time`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("issisiiisiii",$id_user,$nameQuest,$date_of_create,$approval, $descriptionQuest, $id_type, $rating, $id_city, $imgQuest, $numberOfStages,$id_rewards, $time);
        $preparedStatement->execute();
        	//die(var_dump($preparedStatement));
        $result=$preparedStatement->affected_rows;  
         

		if (!$result) 
			return false; 
		else 
		{
			$preparedStatement->close();
			return true;
		}

	 }

	 public function set_stage($stage,$number_stage, $id_quest, $img_stage)
	 {
	 	//move_uploaded_file($img['tmp_name'], $imgDirStage);
	 	$number_stage++;

	 	if (!($preparedStatement = $this->db->prepare('INSERT into stage(id_quest,name, `number`,description, `point`, img, `text`) VALUES (?,?,?,?,?,?,?)'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("isissss",$id_quest,$stage['name_stage'],$number_stage, $stage['description_stage'], $stage['point'], $img_stage, $stage['answer']);
        $preparedStatement->execute();
        
        $result=$preparedStatement->affected_rows;  
         

		if (!$result) 
			return false; 
		else 
		{
			$preparedStatement->close();
			return true;
		}

	 }

	 public function get_id_quest($name)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT id_quest FROM quests WHERE name = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("s", $name);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			return $result->fetch_array(MYSQLI_ASSOC);
		}

	 }

	 public function get_id_stage($name)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT id_quest FROM quests WHERE name = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("s", $name);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$id_stage = $result->fetch_array(MYSQLI_ASSOC);
			return $id_stage;
		}

	 }

	 public function get_status($id_user,$id_stage)
	 {
	 	$id_status = $this->sel_passing($id_user, $id_stage);
	 	if (!$id_status) {
	 		return 'neutral';
	 	}

	 	if (!($preparedStatement = $this->db->prepare('SELECT description FROM status WHERE id_status =  ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("i", $id_status['id_status']);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return 'neutral'; 
		else 
		{
			$status = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $status['description'];
		}

	 }

	 public function getCreated($id_user)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM quests WHERE id_user = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("i", $id_user);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return 'neutral'; 
		else 
		{
			$status = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $status['description'];
		}

	 }

	 public function sel_passing($id_user,$id_stage)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT id_status FROM passing WHERE id_user = ? and id_stage =  ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("ii", $id_user, $id_stage);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$id_status = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $id_status;
		}

	 }

	 public function set_rewards($descriptionRewards, $textRewards, $imgRewards)
	 {
	 	$id_achievements = 1;
	 	if (!($preparedStatement = $this->db->prepare('INSERT into rewards(id_achievements, description, `text`, img) VALUES (?,?,?,? )'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}
  		$preparedStatement->bind_param("isss",$id_achievements, $descriptionRewards,  $textRewards, $imgRewards);
        $preparedStatement->execute();
        $result=$preparedStatement->affected_rows;  
         

		if (!$result) 
			return false; 
		else 
		{
			$query = "SELECT MAX(id_rewards) FROM rewards";
			$result = mysqli_query($this->db, $query);

			$id_rewards = mysqli_fetch_assoc($result);
			$preparedStatement->close();
			return $id_rewards;
		}

	 }

	 public function ins_passing( $id_stage, $id_user, $date_of_begin)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM passing WHERE id_user = ? and id_stage =  ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("ii", $id_user, $id_stage);
        $preparedStatement->execute();
         
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$data = $result->fetch_array(MYSQLI_ASSOC);
			switch($data['id_status']){
        case 1 :
        	$id_status = 3;
        	if (!($preparedStatement = $this->db->prepare('UPDATE passing SET date_of_begin = ?, id_status = ? WHERE id_user = ? and id_stage = ? '))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("siii",$date_of_begin, $id_status, $id_user, $id_stage);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else 
				return true;
           break;
        case 2:
           return false;
           break;
        case 3:
           return false;
           break;
        default:
        	$elected = 0;
        	$id_status = 3;
        	if (!($preparedStatement = $this->db->prepare('INSERT into passing(id_user,id_stage,elected,id_status,date_of_begin) VALUES (?,?,?,?,?);'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("iiiis",  $id_user, $id_stage, $elected ,$id_status ,$date_of_begin);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return true;}
           	break;
        }
			$preparedStatement->close();
		return $id_status;
	}
	 	
	 }

	 public function quest_end($id_stage, $date_of_end,$id_user)
	 {
	 	$id_status = 2;
	 	if (!($preparedStatement = $this->db->prepare('UPDATE passing SET date_of_end = ?, id_status = ? WHERE id_stage = ? and id_user = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("siii",$date_of_end, $id_status, $id_stage, $id_user, $id_stage,$date_of_begin);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else{ 
				$preparedStatement->close();
				return true;}
	 }

	 public function up_passing($column, $update, $id_stage)
	 {
	 	if (!($preparedStatement = $this->db->prepare('UPDATE passing SET ? = ? WHERE id_stage = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("ssi",$column, $update, $id_stage);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return true;
			}

	 }

	 public function sign_in($email, $password)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM user WHERE email = ? and password = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("ss",$email, $password);
        $preparedStatement->execute();
        
        $result=$preparedStatement->get_result();
         

		if (!$result) 
			return false; 
		else 
		{
			$preparedStatement->close();
			return true;
		}

	 }

	 public function up_token($email,$token)
	 {
	 	if (!($preparedStatement = $this->db->prepare('UPDATE user SET token = ? WHERE email = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("ss",$token, $email);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return true;
			}

	 }

	 public function update($token, $column, $update)
	 {
	 	if (!($preparedStatement = $this->db->prepare('UPDATE user SET ? = ? WHERE token =  ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("sss",$column, $update, $token);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return true;
			}

	 }

	 public function selpas($token)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT password FROM user WHERE token = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("s",$token);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->get_result();  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return $result->fetch_array(MYSQLI_ASSOC);
			}

	 }

	 public function questFeed()
	 {
	 	$query = "SELECT * FROM quests ORDER BY id_quest DESC LIMIT 5";
		$result = mysqli_query($this->db, $query);
		for ($i=0; $i < 5; $i++) { 
			$quest[$i] = mysqli_fetch_assoc($result);
		}
		return $quest;
	 }

	 public function creator($id)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT id_user FROM quests WHERE id_quest= ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("i",$id);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->get_result();  
         

			if (!$result) 
				return false; 
			else 
			{
				
				$data = $result->fetch_array(MYSQLI_ASSOC);
				$preparedStatement->close();
				return $data;
			}

	 }

	 public function creatorInfo($id)
	 {

		if (!($preparedStatement = $this->db->prepare('SELECT * FROM user WHERE id_user= ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("i",$id);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->get_result();  
         

			if (!$result) 
				return false; 
			else 
			{
				
				$data = $result->fetch_array(MYSQLI_ASSOC);
				$preparedStatement->close();
				return $data;
			}
	 }

	 public function getFavourites($id_user)
	 {
	 	$elected = 1;
	 	if (!($preparedStatement = $this->db->prepare('SELECT id_stage FROM passing WHERE id_user = ? and elected = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("ii",$id_user, $elected);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->get_result();  
         

			if (!$result) 
				return false; 
			else 
			{
				while($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$id_stage[] = $row;
				}
				$data = $result->fetch_array(MYSQLI_ASSOC);
				
				for ($i=0; $i < count($id_stage); $i++) 
				{
					$id_stages = $id_stage[$i]['id_stage'];
					
					if (!($preparedStatement = $this->db->prepare('SELECT id_quest FROM stage WHERE id_stage = ?'))) {
      					die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  					}

  					$preparedStatement->bind_param("i",$id_stages);
        			$preparedStatement->execute();
        
        			$result=$preparedStatement->get_result();  
         

					$id_quest[] = $result->fetch_array(MYSQLI_ASSOC);


				}

				for ($i=0; $i < count($id_quest); $i++) 
				{ 
					$id_quests = $id_quest[$i]['id_quest'];

					if (!($preparedStatement = $this->db->prepare('SELECT * FROM quests WHERE id_quest = ?'))) {
      					die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  					}

  					$preparedStatement->bind_param("i",$id_quest);
        			$preparedStatement->execute();
        
        			$result=$preparedStatement->get_result();  

					$favourites[] = $result->fetch_array(MYSQLI_ASSOC);
			
				}

				if($favourites)
				{
					$preparedStatement->close();
					return $favourites;
				}
				else
				{
					return false;
				}
			}
	 	
	 }

	 public function getStory($id_user)
	 {	
	 	if (!($preparedStatement = $this->db->prepare("SELECT id_stage FROM passing WHERE id_user = ? and NULLIF(date_of_begin, '') IS NOT NULL"))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("i",$id_user);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->get_result();  
         

			if (!$result) 
				return false; 
			else 
			{
				while($row = $result->fetch_array(MYSQLI_ASSOC))
				{
					$id_stage[] = $row;
				}
				
				for ($i=0; $i < count($id_stage); $i++) 
				{
					$id_stages = $id_stage[$i]['id_stage'];
					
					if (!($preparedStatement = $this->db->prepare('SELECT id_quest FROM stage WHERE id_stage = ?'))) {
      					die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  					}

  					$preparedStatement->bind_param("i",$id_stages);
        			$preparedStatement->execute();
        
        			$result=$preparedStatement->get_result();  
         

					$id_quest[] = $result->fetch_array(MYSQLI_ASSOC);


				}

				for ($i=0; $i < count($id_quest); $i++) 
				{ 
					$id_quests = $id_quest[$i]['id_quest'];

					if (!($preparedStatement = $this->db->prepare('SELECT * FROM quests WHERE id_quest = ?'))) {
      					die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  					}

  					$preparedStatement->bind_param("i",$id_quest);
        			$preparedStatement->execute();
        
        			$result=$preparedStatement->get_result();  

					$story[] = $result->fetch_array(MYSQLI_ASSOC);
			
				}
				if($story)
				{
					$preparedStatement->close();
					return $story;
				}
				else
				{
					return false;
				}
			}

	 }

	 public function search($column, $el)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM quests WHERE id_quest = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("i",$id_quest);
        $preparedStatement->execute();
        
        $result=$preparedStatement->get_result();  

		if(!$result)
			{return false;}
		else
		{
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$search[] = $row;
			}
			$preparedStatement->close();
			return $search;
		}

	 }

	 public function getRefs($id_quest)
	 {
	 	$number = 1;
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM stage WHERE id_quest = ? and `number` = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("ii",$id_quest, $number);
        $preparedStatement->execute();
        
        $result=$preparedStatement->get_result();  

		if(!$result)
			{return false;}
		else
		{
			$refs = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $refs;
		}

	 }

	 public function getRate($id_quest)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT rating, approval FROM quests WHERE id_quest = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("i",$id_quest);
        $preparedStatement->execute();
        
        $result=$preparedStatement->get_result();  

		if(!$result)
			{return false;}
		else
		{
			$refs = $result->fetch_array(MYSQLI_ASSOC);
			$preparedStatement->close();
			return $refs;
		}
	 }

	 public function rateQuest($id_quest,$rate,$appreciated)
	 {
	 	$query = "UPDATE quests SET rating = '$rate', appreciated = '$appreciated' WHERE id_quest = '$id_quest'";
		$result = mysqli_query($this->DB, $query);
		if ($result==1) 
		{
			return true;
		}
		else
		{
			$preparedStatement->close();
			return false;
		}
	 }

	 public function addFavourite($id_user,$id_stage)
	 {
	 	if (!($preparedStatement = $this->db->prepare('SELECT * FROM passing WHERE id_user = ? and id_stage = ?'))) {
      		die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  		}

  		$preparedStatement->bind_param("ii",$id_user ,$id_stage);
        $preparedStatement->execute();
        
        $result=$preparedStatement->get_result();  

		if(!$result)
		{
			$elected = 1;
			if (!($preparedStatement = $this->db->prepare('UPDATE passing SET elected = ? WHERE id_user = ? and id_stage = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("iii",$elected,$id_user, $id_stage);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else 
				return true;
		}
		else
		{
			$elected = 1;
			$id_status = 1;
			if (!($preparedStatement = $this->db->prepare('INSERT into passing(id_user,id_stage,elected,id_status) VALUES (?,?,1,1)'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("iiii",$id_user, $id_stage, $elected, $id_status);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return true;
			}
		}

	}

	public function up_img($src,$img)
	{
		move_uploaded_file($img['img']['tmp_name'], $src);

		if (!($preparedStatement = $this->db->prepare('UPDATE user SET img = ? WHERE id_user = ?'))) {
      			die( "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error);
  			}

  			$preparedStatement->bind_param("s",$src);
        	$preparedStatement->execute();
        
        	$result=$preparedStatement->affected_rows;  
         

			if (!$result) 
				return false; 
			else {
				$preparedStatement->close();
				return true;
			}

	}

	public function verificathion_img($img)
	{		
		$type = explode('/', $img['type']);
		if ($type[0] != 'image')
		{
			return 'Загрузите картинку';
		}
		if ($img['size'] > 250000)
		{
			return 'Картинка превышает допустимый размер 250 Кб';	    
		}

		return false;
	}
}
