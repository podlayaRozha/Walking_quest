<?php
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
require_once 'core/auth.php';
try {
	Route::start(); // запускаем маршрутизатор
} catch(Exception $e){
	Route::ErrorPage404();
}
?>