function initMap(map) {
  let insertMap =document.getElementById(map);
    var defaultCenter = {lat: 47.236020, lng: 39.699150};
    var map = new google.maps.Map(insertMap, 
    {
      zoom: 11,
      center: defaultCenter
    });
  //Прежде чем пытаться как-то работать с маркером, его бы хорошо объявить. Иначе будет ругаться. Ну и глобально сделал для дого, что бы потом координаты можно было получить в любом месте кода
  var marker;
  //Слушатель нажатия на какое-либо место на карте
  //я просто перенес его в initMap, ибо вне него он будет жаловатся на остутвие переменной google  
  google.maps.event.addListener(map, 'click', function(event) 
    {
      latlng = event.latLng;
      let lat = latlng.lat();
      let lng = latlng.lng();
      let coordinates = lat+", "+lng;
     
    //Если маркет уже есть, то просто меняем его местоположение на новое при клике в другое место
      if (marker != null) 
        {
          marker.setMap(map); //Устанавливает на какой картке маркер отображается(грубо говоря, делает его видимым)
          marker.setPosition(latlng);
    } 
    else  
        {  //А если маркера еще нет(первый раз кликнули) - создаем)
          marker = new google.maps.Marker(
            {
              position: latlng,
              clickable: true,
              map: map,
              title: 'Чтобы убрать маркер с карты, повторно нажмите на него', //Это подсказка, которая будет отображатся при наведении на маркер. Тут хорошо бы что-то осмысленное поставить - например, что при клике на маркер он удалится
              animation: google.maps.Animation.DROP, //Анимация, с которой маркет 
              visible: true
            });
        }
    //Вот эта штука по клику на сам маркет убирает его. Можно "click" заменить на "dblclick", тогда будет по двойному клику
      marker.addListener("click", function() 
      {
        marker.setMap(null); //Ставит маркеру карту в null. Ну то есть маркер исчезает
        marker.position = null; //На всякий случаем обнуляем маркеру местоположение. Например, что бы при попытке отправить форму с удаленным маркером не прилетело чего линего
      });
    console.log("Координаты маркера: " + marker.position); //Для отладочки можно в консоли писать куда конкретно ткнули. Можно и убарть. Заодно, вот так ты можешь получить потом координаты: marker.position
  });
  }