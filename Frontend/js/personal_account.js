$( document ).ready(function() {
    $('#list-create').show()
    $('#list-favourits').hide()
    $('#list-traversed').hide()
    $('#setings').hide()
    
    $.ajax({
        url: "https://quest-area.com/api/getCreated",
        type: "POST",
        success: function(data) {
            let obj = jQuery.parseJSON(data);
            let count = Object.keys(obj).length;
            for(let i = 0; i < count; i++)
            {
                $("#list-create").append(`<figure id="elment_list">
                <img id="img_user" alt="Изображение к квесту" src=\"` + obj["quest" + i].image + `"/>
                <figcaption>
                    <div>
                    <p id="name_quest">` + obj["quest" + i].questname + `</p>
                    <p id="rating">Рейтинг:` + obj["quest" + i].rating + `</p>
                </div>
                    <div>
                    <button id="delit_list" class="button_cards">
                        <svg xmlns="http://www.w3.org/2000/svg" height="80%" viewBox="0 0 448 512">
                            <path class="s2" d="M0 84V56c0-13.3 10.7-24 24-24h112l9.4-18.7c4-8.2 12.3-13.3 21.4-13.3h114.3c9.1 0 17.4 5.1 21.5 13.3L312 32h112c13.3 0 24 10.7 24 24v28c0 6.6-5.4 12-12 12H12C5.4 96 0 90.6 0 84zm415.2 56.7L394.8 467c-1.6 25.3-22.6 45-47.9 45H101.1c-25.3 0-46.3-19.7-47.9-45L32.8 140.7c-.4-6.9 5.1-12.7 12-12.7h358.5c6.8 0 12.3 5.8 11.9 12.7z"/></svg>
                    </button>
                    <button id="edit_list" class="button_cards">
                        <svg xmlns="http://www.w3.org/2000/svg" height="80%" viewBox="0 0 448 512">
                            <path class="s2" d="M400 480H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48v352c0 26.5-21.5 48-48 48zM238.1 177.9L102.4 313.6l-6.3 57.1c-.8 7.6 5.6 14.1 13.3 13.3l57.1-6.3L302.2 242c2.3-2.3 2.3-6.1 0-8.5L246.7 178c-2.5-2.4-6.3-2.4-8.6-.1zM345 165.1L314.9 135c-9.4-9.4-24.6-9.4-33.9 0l-23.1 23.1c-2.3 2.3-2.3 6.1 0 8.5l55.5 55.5c2.3 2.3 6.1 2.3 8.5 0L345 199c9.3-9.3 9.3-24.5 0-33.9z"/></svg>
                    </button>
                </div>
            
                </figcaption>
            </figure>`);
            }
        },
        fail: function(data){
            $("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
        }
      });

      $.ajax({
        url: "https://quest-area.com/api/getFavourites",
        type: "POST",
        success: function(data) {
            let obj = jQuery.parseJSON(data);
            let count = Object.keys(obj).length;
            for(let i = 0; i < count; i++)
            {
                $("#list-favourits").append(`<figure id="elment_list">
                <img id="img_user" alt="Изображение к квесту" src=\"` + obj["quest" + i].image + `"/>
                <figcaption>
                    <div>
                    <p id="name_quest">` + obj["quest" + i].questname + `</p>
                    <p id="rating">Рейтинг` + obj["quest" + i].rating + `</p>
                </div>
                </figcaption>
            </figure>`);
            }
        },
        fail: function(data){
            $("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
        }
      });

      $.ajax({
        url: "https://quest-area.com/api/getStory",
        type: "POST",
        success: function(data) {
            let obj = jQuery.parseJSON(data);
            let count = Object.keys(obj).length;
            for(let i = 0; i < count; i++)
            {
                $("#list-traversed").append(`<figure id="elment_list">
                <img id="img_user" alt="Изображение к квесту" src=\"` + obj["quest" + i].image + `"/>
                <figcaption>
                    <div>
                    <p id="name_quest">` + obj["quest" + i].questname + `</p>
                    <p id="rating"> Рейтинг:` + obj["quest" + i].rating + `</p>
                </div>
                </figcaption>
            </figure>`);
            }
        },
        fail: function(data){
            $("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
        }
      });
});

$('#create').click(function()
    {
    $('#list-create').toggle('norml')
    $('#list-favourits').hide('norml')
    $('#list-traversed').hide('norml')
    }

)
$('#favourits').click(function()
    {
        $('#list-favourits').toggle('norml')
        $('#list-create').hide('norml')
        $('#list-traversed').hide('norml')
    }
)
$('#traversed').click(function()
    {
        $('#list-traversed').toggle('norml')
        $('#list-favourits').hide('norml')
        $('#list-create').hide('norml')
    }
)

$('#setings_button').click(function()
    {
        $('#setings').toggle('norml')
    }
)

$('#exet_button').click(function() {
    event.preventDefault();
    $.ajax({
        url: "https://quest-area.com/api/logout",
        type: "POST",
        success: function(data) {
            var url = "https://quest-area.com/";
            $(location).attr('href',url);
        },
        fail: function(data){
            $("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
        }
      });
})

/*Смена логина*/
$("#change_name").submit(function(){
  	event.preventDefault();
  	let $form = $(this);
    let serializedData = $form.serialize();
  	serializedData = serializedData + "&column=login";              /*&column=login указывает какое поле менять.*/
	$.ajax({
        url: "https://quest-area.com/api/update",
        type: "POST",
      	data: serializedData,
        success: function(data) {
            $("#change_name").append('<p>Логин изменен.</p>');
        },
        fail: function(data){
            //$("#sUp").append('<p>Не удалось подключится к серверу! Пожалуйста, попробуйте снова.</p>');
        }
      });
})
